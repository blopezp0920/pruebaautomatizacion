package co.com.sofka.util;

public enum RutasJson {

    RUTAS_JSON_INICIO_SESION_ADMIN("src/test/resources/archivosjson/CredencialesAdmin.json"),
    RUTAS_JSON_LISTADO_BOOKINGS_POR_ID("src/test/resources/archivosjson/BookingSid.json"),
    RUTAS_JSON_INGRESO_DE_UN_NUEVO_BOOKING("src/test/resources/archivosjson/NuevosBookings.json");

    private final String value;

    public String getValue() {
        return value;
    }

    RutasJson(String value) {
        this.value = value;
    }

}
