package co.com.sofka.util;

import co.com.sofka.modelo.ModeloBooking;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.*;
import java.util.Random;

public class IngresarDatosArchivosJson {

    private static final Logger LOGGER = Logger.getLogger(IngresarDatosArchivosJson.class);
    private static final int CONTADOR_DE_RESGISTROS = 0;
    private static final int NUMERO_MAXIMO_DE_ELEMENTOS = 4;
    private static final int VALOR_MINIMO_PARA_GENERAR_RANDOM = 1;
    private static FileWriter fileBookingsId;
    private  JSONObject jsonRegistro;
    BufferedWriter borrarContenidoArchivo = new BufferedWriter(new FileWriter(RutasJson.RUTAS_JSON_LISTADO_BOOKINGS_POR_ID.getValue()));

    public JSONObject getJsonRegistro() {
        return jsonRegistro;
    }

    public void setJsonRegistro(JSONObject jsonRegistro) {
        this.jsonRegistro = jsonRegistro;
    }

    private final Random random = new Random();
    static {
        try {
            fileBookingsId = new FileWriter(RutasJson.RUTAS_JSON_LISTADO_BOOKINGS_POR_ID.getValue());

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public IngresarDatosArchivosJson() throws IOException {
        // Es una sugerencia que me hace Java
    }


    public void ingresarDatosAlArchivoJson(String datos) throws IOException {

        borrarContenidoArchivo.write("");
        borrarContenidoArchivo.close();
        try  {
            fileBookingsId.write(datos);
            fileBookingsId.flush();
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
        }
    }

    public JSONArray obtenenerListadoBookingsId() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        return (JSONArray) parser.parse(new FileReader(RutasJson.RUTAS_JSON_LISTADO_BOOKINGS_POR_ID.getValue()));

    }

    public JSONObject leerJsonRegistroBooking() throws IOException, ParseException {

        int contador = CONTADOR_DE_RESGISTROS;
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayRegistrosBookings = (JSONArray) parser.parse(new FileReader(RutasJson.RUTAS_JSON_INGRESO_DE_UN_NUEVO_BOOKING.getValue()));
        JSONObject myObjectPrueba = new JSONObject();
        ModeloBooking modeloBooking = new ModeloBooking();
        JSONObject subdata;
        int valorAleatorioRegistro = random.nextInt(NUMERO_MAXIMO_DE_ELEMENTOS + VALOR_MINIMO_PARA_GENERAR_RANDOM) + VALOR_MINIMO_PARA_GENERAR_RANDOM;
        for (Object objeto : jsonArrayRegistrosBookings)
        {
            JSONObject jsonRegistrosBookings = (JSONObject) objeto;
            contador++;
            if (contador == valorAleatorioRegistro){
                subdata = (JSONObject) jsonRegistrosBookings.get("bookingdates");
                modeloBooking.setNombre((String) jsonRegistrosBookings.get("firstname"));
                modeloBooking.setApellido((String) jsonRegistrosBookings.get("lastname"));
                modeloBooking.setDeposito((Boolean) jsonRegistrosBookings.get("depositpaid"));
                modeloBooking.setNecesidadesAdicionales((String) jsonRegistrosBookings.get("additionalneeds"));
                modeloBooking.setPrecioTotal(Math.toIntExact((long) jsonRegistrosBookings.get("totalprice")));
                myObjectPrueba.put("firstname",modeloBooking.getNombre());
                myObjectPrueba.put("lastname",modeloBooking.getApellido());
                myObjectPrueba.put("totalprice",modeloBooking.getPrecioTotal());
                myObjectPrueba.put("depositpaid",modeloBooking.isDeposito());
                myObjectPrueba.put("bookingdates",subdata);
                myObjectPrueba.put("additionalneeds",modeloBooking.getNecesidadesAdicionales());

            }
        }
        return myObjectPrueba;

    }



}
