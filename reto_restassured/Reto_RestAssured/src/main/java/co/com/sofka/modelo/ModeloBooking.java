package co.com.sofka.modelo;
public class ModeloBooking {

    private String nombre;
    private String apellido;
    private int precioTotal;
    private boolean deposito;
    private String necesidadesAdicionales;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public float getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(int precioTotal) {
        this.precioTotal = precioTotal;
    }

    public boolean isDeposito() {
        return deposito;
    }

    public void setDeposito(boolean deposito) {
        this.deposito = deposito;
    }

    public String getNecesidadesAdicionales() {
        return necesidadesAdicionales;
    }

    public void setNecesidadesAdicionales(String necesidadesAdicionales) {
        this.necesidadesAdicionales = necesidadesAdicionales;
    }
}
