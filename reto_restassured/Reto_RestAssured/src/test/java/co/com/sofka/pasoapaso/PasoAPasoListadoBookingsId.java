package co.com.sofka.pasoapaso;

import co.com.sofka.configuracion.Configuracion;
import co.com.sofka.util.IngresarDatosArchivosJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class PasoAPasoListadoBookingsId extends Configuracion {

    private static final Logger LOGGER = Logger.getLogger(PasoAPasoListadoBookingsId.class);
    private Response response;
    private RequestSpecification request;

    @Dado("que el suaurio se encunetra en el apartado de listar bookings")
    public void queElSuaurioSeEncunetraEnElApartadoDeListarBookings() {
        try {
            generalSetup();
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON);
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }

    }
    @Cuando("el usuario hace la petición GET")
    public void elUsuarioHaceLaPeticionGET() {

        try {
            response = request.when()
                    .log()
                    .all()
                    .get(BOOKING_RESOURCE);
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }

    }
    @Entonces("el sistema listará todas las existencias de los bookings por su respectivo identificador")
    public void elSistemaListaraTodasLasExistenciasDeLosBookingsPorSuRespectivoIdentificador() throws IOException, ParseException {
        IngresarDatosArchivosJson ingresarDatosArchivosJson = new IngresarDatosArchivosJson();
        ingresarDatosArchivosJson.ingresarDatosAlArchivoJson(response.getBody().prettyPrint());
        response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_OK)
                .body(anything(ingresarDatosArchivosJson.obtenenerListadoBookingsId().toJSONString()));
    }

}
