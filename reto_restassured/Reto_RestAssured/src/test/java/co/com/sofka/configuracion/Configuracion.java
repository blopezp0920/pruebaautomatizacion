package co.com.sofka.configuracion;

import io.restassured.RestAssured;
import org.apache.log4j.PropertyConfigurator;

import static co.com.sofka.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;

public class Configuracion {

    private static final String BASE_URI = "https://restful-booker.herokuapp.com";
    protected static final String BOOKING_RESOURCE = "/booking";
    protected static final String INICIO_DE_SESION_RESOURCE = "/auth";

    protected void generalSetup(){
        setUpLog4j2();
        setUpBases();
    }
    private void setUpLog4j2(){
        PropertyConfigurator.configure(LOG4J_PROPERTIES_FILE_PATH.getValue());
    }

    private void setUpBases(){
        RestAssured.baseURI = BASE_URI;
    }

}
