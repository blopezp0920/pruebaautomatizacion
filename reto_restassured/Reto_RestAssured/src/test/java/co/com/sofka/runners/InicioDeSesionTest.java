package co.com.sofka.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/InicioDeSesion.feature",
                    "src/test/resources/features/ObtenerListadoDeBookings.feature",
                    "src/test/resources/features/IngresarUnNuevoBooking.feature"},
        glue = {"co.com.sofka.pasoapaso"}
)
public class InicioDeSesionTest {

}
