package co.com.sofka.pasoapaso;

import co.com.sofka.configuracion.Configuracion;
import co.com.sofka.util.RutasJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.Assertions;
import java.io.FileReader;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.notNullValue;

public class PasoAPasoInicioDeSesionTest extends Configuracion {

    private static final Logger LOGGER = Logger.getLogger(PasoAPasoInicioDeSesionTest.class);
    private Response response;
    private RequestSpecification request;

    @SneakyThrows
    @Dado("que el usuario se encuntra en la página de inicio de sesión he ingresa el usuario y la contraseña")
    public void queElUsuarioSeEncuntraEnLaPaginaDeInicioDeSesionHeIngresaElUsuarioYLaContrasenia() {
        try {
            Object objetoJsonInicioDeSesion = new JSONParser().parse(new FileReader(RutasJson.RUTAS_JSON_INICIO_SESION_ADMIN.getValue()));
            JSONObject jsonInicioDeSesion = (JSONObject) objetoJsonInicioDeSesion;
            generalSetup();
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON)
                    .body(jsonInicioDeSesion
                    );
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @Cuando("el usuario hace la petición")
    public void elUsuarioHaceLaPeticion() {

            try {
                response = request.when()
                        .log()
                        .all()
                        .post(INICIO_DE_SESION_RESOURCE);
            } catch (Exception e){
                LOGGER.error(e.getMessage(), e);
                Assertions.fail(e.getMessage());
            }
        }

    @Entonces("el sistema retornará un token de respuesta indicando una respuesta exitosa")
    public void elSistemaRetornaraUnTokenDeRespuestaIndicandoUnaRespuestaExitosa() {
        response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_OK)
                .body(notNullValue());
    }
}
