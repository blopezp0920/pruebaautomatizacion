package co.com.sofka.pasoapaso;

import co.com.sofka.configuracion.Configuracion;
import co.com.sofka.util.IngresarDatosArchivosJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import java.io.IOException;
import static io.restassured.RestAssured.given;

public class PasoAPasoRegistroNuevoBooking extends Configuracion {

    private static final Logger LOGGER = Logger.getLogger(PasoAPasoInicioDeSesionTest.class);
    private Response response;
    private RequestSpecification request;
    IngresarDatosArchivosJson ingresarDatosArchivosJson = new IngresarDatosArchivosJson();

    public PasoAPasoRegistroNuevoBooking() throws IOException {
    }

    @Dado("que el usuario se encuentra en el apartado de registro de un nuevo Booking")
    public void queElUsuarioSeEncuentraEnElApartadoDeRegistroDeUnNuevoBooking() {
        try {
            generalSetup();
            ingresarDatosArchivosJson.setJsonRegistro(ingresarDatosArchivosJson.leerJsonRegistroBooking());
            request = given()
                    .log()
                    .all()
                    .contentType(ContentType.JSON)
                    .body(ingresarDatosArchivosJson.getJsonRegistro()
                    );
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }
    }
    @Cuando("el usuario hace la petición de registrar el nuevo Booking")
    public void elUsuarioHaceLaPeticionDeRegistrarElNuevoBooking() {

        try {
            response = request.when()
                    .log()
                    .all()
                    .post(BOOKING_RESOURCE);
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            Assertions.fail(e.getMessage());
        }

    }
    @Entonces("El sistema retornará la información ingresada indicando una respuesta exitosa")
    public void elSistemaRetornaraLaInformacionIngresadaIndicandoUnaRespuestaExitosa() {
        response.then()
                .log()
                .all()
                .statusCode(HttpStatus.SC_OK)
                .body("booking.firstname", Matchers.is(ingresarDatosArchivosJson.getJsonRegistro().get("firstname")));
    }

}
