#language: es

Característica: Listado de Bookings por identificación
  Como usuario del sistema
  quiero listar todos los bookings
  para poder verificar que existencias bokings hay

  Escenario: Listado de bookings exitoso
    Dado que el suaurio se encunetra en el apartado de listar bookings
    Cuando el usuario hace la petición GET
    Entonces el sistema listará todas las existencias de los bookings por su respectivo identificador