#language: es

Característica: Registro de un nuevo Booking
  Como usuario registrado en el sistema
  necesito registrar un nuevo Booking
  para ingresar la información pertinnte al Booking

  Escenario: Registro de un nuevo Booking exitoso
    Dado que el usuario se encuentra en el apartado de registro de un nuevo Booking
    Cuando el usuario hace la petición de registrar el nuevo Booking
    Entonces El sistema retornará la información ingresada indicando una respuesta exitosa