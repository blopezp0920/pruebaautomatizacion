#language: es

  Característica: Inicio de sesión
  Como usuario administrador registrado en el sistema
  necesito validar la operación de logueo
  para poder interactuar con los diferentes apartados del sitio

  Escenario: Inicio de sesión exitoso
    Dado que el usuario se encuntra en la página de inicio de sesión he ingresa el usuario y la contraseña
    Cuando el usuario hace la petición
    Entonces el sistema retornará un token de respuesta indicando una respuesta exitosa
