# language: es

@FeatureName:eliminarUnProductoSeleccionado
  Característica: Eliminar producto seleccionado
    como usuario
    quiero eleiminar un producto seleccionado en el carrito de compras
    para evitar la compra de un producto que no quiera comprar

  Escenario: Eliminar un producto del carritos de compras exitoso
    Dado que el usuario esté logueado y haya seleccionado los productos
    Cuando el usuario ingresa al carrito de compras y elimina un producto
    Entonces entonces el producto desaparecerá del carrito de compras