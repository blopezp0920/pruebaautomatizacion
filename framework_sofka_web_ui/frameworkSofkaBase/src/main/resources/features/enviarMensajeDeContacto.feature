# language: es

@FeatureName:enviarMensajeDeContacto
  Característica: Enviar mensaje de contacto
    Como usuario de la plataforma
    necesito enviar un mensaje de contacto
    Para hacer algún tipo de reclamo respecto a algún artículo

  @Scenario:enviarMensajeContactoExitoso
  Escenario: Mensaje de contacto enviado exitosamente
    Dado que el usuario navegó hasta el apartado de contacto
    Cuando el usuario suministra los campos obligatorios correctamente
    Entonces el sistema mostrará que el mensaje ha sido enviado exitosamente

    @Scenario:mensajeDeContactoNoExitoso
    Escenario: Mensaje de contacto no exitoso
      Dado que el usuario navegó al apartado de contactos
      Cuando que el usuario suministró los campos exceptuando el campo del email
      Entonces el sistema mostrará un mensaje de error