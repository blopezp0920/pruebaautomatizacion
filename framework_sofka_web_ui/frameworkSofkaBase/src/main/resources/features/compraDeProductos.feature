# language: es

  @FeatureName:compraDeProductos
  Característica: Compra de productos
    Como usuario
    necesito agregar productos al carrito de compras
    Para proceder con la compra de los productos seleccionados

  Escenario: Compra de productos exitosa
    Dado que el usuario esté logueado
    Cuando el usuario selecciona productos de la página principal y valida
    Entonces se mostrará un mensaje que la compra ha sido efectuada

