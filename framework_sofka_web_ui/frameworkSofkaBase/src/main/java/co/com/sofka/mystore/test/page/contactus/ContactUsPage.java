package co.com.sofka.mystore.test.page.contactus;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactUsPage {

    @CacheLookup
    @FindBy(xpath = "/html/body/div/div[2]/div/div[3]/div/form/fieldset/div[1]/div[1]/div[1]/div/select")
    WebElement subjectHeading;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"email\"]")
    WebElement email;

    @CacheLookup
    @FindBy(xpath = "/html/body/div/div[2]/div/div[3]/div/form/fieldset/div[1]/div[1]/div[2]/input")
    WebElement orderReference;

    @CacheLookup
    @FindBy(id = "message")
    WebElement message;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"submitMessage\"]/span")
    WebElement send;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"center_column\"]/p")
    WebElement messageSuccess;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"center_column\"]/div/ol/li")
    WebElement messageError;

    public WebElement getMessageError() {
        return messageError;
    }

    public WebElement getMessageSuccess() {
        return messageSuccess;
    }

    public WebElement getSend() {
        return send;
    }

    public WebElement getSubjectHeading() {
        return this.subjectHeading;
    }

    public WebElement getEmail() {
        return this.email;
    }

    public WebElement getOrderReference() {
        return this.orderReference;
    }

    public WebElement getMessage() {
        return this.message;
    }

    public ContactUsPage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }
}
