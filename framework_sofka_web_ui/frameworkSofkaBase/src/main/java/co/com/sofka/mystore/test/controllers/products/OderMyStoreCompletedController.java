package co.com.sofka.mystore.test.controllers.products;

import co.com.sofka.mystore.test.page.contactus.ContactUsPage;
import co.com.sofka.mystore.test.page.products.ProductsPage;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Report;

import static co.com.sofka.mystore.test.helpers.Seconds.FIVE_SECONDS;

public class OderMyStoreCompletedController {

    private WebAction webAction;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public String pedidoCompletado(){
        String message = "";
        try {
            ProductsPage productsPage = new ProductsPage(webAction.getDriver());
            message = webAction.getText(productsPage.getOrderComplete(),FIVE_SECONDS.getValue(),true);

        } catch (Exception e) {
            Report.reportFailure("Ocurrió un error al intentar mandar el mensaje de contacto.", e);
        }
        return message;
    }

}

