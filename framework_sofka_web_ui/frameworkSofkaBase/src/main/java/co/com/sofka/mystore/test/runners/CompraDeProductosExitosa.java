package co.com.sofka.mystore.test.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = SnippetType.CAMELCASE,
        features = {"src/main/resources/features/compraDeProductos.feature"},
        glue = {"co.com.sofka.mystore.test.stepdefinition.compradeproductos"},
        tags = ""
)
public class CompraDeProductosExitosa {
}
