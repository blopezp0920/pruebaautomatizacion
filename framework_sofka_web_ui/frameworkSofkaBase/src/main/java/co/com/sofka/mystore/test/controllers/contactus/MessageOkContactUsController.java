package co.com.sofka.mystore.test.controllers.contactus;

import co.com.sofka.mystore.test.page.contactus.ContactUsPage;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Report;

import static co.com.sofka.mystore.test.helpers.Seconds.FIVE_SECONDS;

public class MessageOkContactUsController {

    private WebAction webAction;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public String obtenerMensajeDeContactoExitoso(){
        String message = "";
        try {
            ContactUsPage contactUsPage = new ContactUsPage(webAction.getDriver());
            message = webAction.getText(contactUsPage.getMessageSuccess(),FIVE_SECONDS.getValue(),true);

        } catch (Exception e) {
            Report.reportFailure("Ocurrió un error al intentar mandar el mensaje de contacto.", e);
        }
        return message;
    }
}
