package co.com.sofka.mystore.test.controllers.contactus;

import co.com.sofka.mystore.test.models.ContactUsModel;
import co.com.sofka.mystore.test.page.contactus.LangingPageContactPage;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.exceptions.WebActionsException;

import static co.com.sofka.mystore.test.helpers.Seconds.TEN_SECONDS;

public class LandingPageContactUsController {

    private WebAction webAction;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public void navegarAlApartadoContactUs() throws WebActionsException {

        LangingPageContactPage langingPageContactPage  = new LangingPageContactPage(webAction.getDriver());
        webAction.click(langingPageContactPage.getContactUs(),TEN_SECONDS.getValue(),true);
    }
}
