package co.com.sofka.mystore.test.stepdefinition.enviarmensajecontacto;

import co.com.sofka.mystore.test.controllers.contactus.FormContactUsController;
import co.com.sofka.mystore.test.controllers.contactus.LandingPageContactUsController;
import co.com.sofka.mystore.test.controllers.contactus.MessageOkContactUsController;
import co.com.sofka.mystore.test.controllers.openwebpage.StartBrowserController;
import co.com.sofka.mystore.test.helpers.TestInfo;
import co.com.sofka.mystore.test.models.ContactUsModel;
import co.com.sofka.mystore.test.stepdefinition.setup.Setup;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;
import co.com.sofka.test.utils.files.PropertiesFile;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import org.junit.BeforeClass;

import java.nio.file.Path;
import java.nio.file.Paths;

import static co.com.sofka.mystore.test.helpers.Dictionary.*;
import static co.com.sofka.mystore.test.helpers.Helper.generateContactUs;

public class EnviarMensajeContactoExitosoStepDefinition extends Setup {

    private WebAction webAction;
    private ContactUsModel contactUsModel;

    @Before
    public void setup(Scenario scenario){
        setupFile();
        testInfo = new TestInfo(scenario);
        webAction = new WebAction(testInfo.getFeatureName());
         contactUsModel = generateContactUs(SPANISH_CODE_LANGUAGE, COUNTRY_CODE, EMAIL_DOMAIN);
    }

    @Dado("que el usuario navegó hasta el apartado de contacto")
    public void queElUsuarioNavegoHastaElApartadoDeContacto() throws WebActionsException {
        StartBrowserController startBrowserWebController = new StartBrowserController();
        startBrowserWebController.setBrowser(browser());
        startBrowserWebController.setWebAction(webAction);
        startBrowserWebController.setFeature(testInfo.getFeatureName());
        startBrowserWebController.abrirTiendaOnline();
        LandingPageContactUsController landingPageContactUsController = new LandingPageContactUsController();
        landingPageContactUsController.setWebAction(webAction);
        landingPageContactUsController.navegarAlApartadoContactUs();
    }

    @Cuando("el usuario suministra los campos obligatorios correctamente")
    public void elUsuarioSuministraLosCamposObligatoriosCorrectamente() {
        FormContactUsController formContactUsController = new FormContactUsController();
        formContactUsController.setContactUsModel(contactUsModel);
        formContactUsController.setWebAction(webAction);
        formContactUsController.llenarCampos();
    }

    @Entonces("el sistema mostrará que el mensaje ha sido enviado exitosamente")
    public void elSistemaMostraraQueElMensajeHaSidoEnviadoExitosamente() {

        MessageOkContactUsController messageOkContactUsController = new MessageOkContactUsController();
        messageOkContactUsController.setWebAction(webAction);

        Assert.Hard
                .thatString(
                        messageOkContactUsController.obtenerMensajeDeContactoExitoso()
                ).isEqualTo(
                        CONTACT_SUCCESS
                );
    }

    @After
    public void cerrarDriver(){

        if (webAction != null && webAction.getDriver() != null)
            webAction.closeBrowser();

        Report.reportInfo("***** PRUEBA FINALIZADA ******"
                .concat(testInfo.getFeatureName())
                .concat("-")
                .concat(testInfo.getScenarioName()));
    }

}
