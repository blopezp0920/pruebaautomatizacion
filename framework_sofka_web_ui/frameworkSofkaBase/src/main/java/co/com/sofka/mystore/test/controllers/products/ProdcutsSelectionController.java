package co.com.sofka.mystore.test.controllers.products;

import co.com.sofka.mystore.test.page.products.ProductsPage;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;
import static co.com.sofka.mystore.test.helpers.Dictionary.NUMBER_PRODUCTS;
import static co.com.sofka.mystore.test.helpers.Dictionary.START_VARIABLES;
import static co.com.sofka.mystore.test.helpers.Seconds.TEN_SECONDS;

public class ProdcutsSelectionController {

    private WebAction webAction;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public void seleccionarProducto(){
        int contador = START_VARIABLES;
        try {
        while (contador != NUMBER_PRODUCTS) {
                ProductsPage productsPage = new ProductsPage(webAction.getDriver());
                webAction.click(productsPage.randomProducts(webAction.getDriver()),TEN_SECONDS.getValue(), true);
                webAction.click(productsPage.getAddToCart(),TEN_SECONDS.getValue(), true);
                webAction.click(productsPage.getClosePage(),TEN_SECONDS.getValue(),true);
                webAction.click(productsPage.getReturnShooping(),TEN_SECONDS.getValue(),true);
            contador++;
            }
            ProductsPage productsPage = new ProductsPage(webAction.getDriver());
        webAction.click(productsPage.getCheckOut(),TEN_SECONDS.getValue(),true);
        webAction.click(productsPage.getProcededToCheckOut(),TEN_SECONDS.getValue(),true);
        webAction.click(productsPage.getProcededToCheckOutAddress(),TEN_SECONDS.getValue(),true);
        webAction.click(productsPage.getTermsOfService(),TEN_SECONDS.getValue(),true);
        webAction.click(productsPage.getProcededToCheckOutShipping(),TEN_SECONDS.getValue(),true);
        webAction.click(productsPage.getPayBank(),TEN_SECONDS.getValue(),true);
        webAction.click(productsPage.getConfirmOrder(),TEN_SECONDS.getValue(),true);
        }catch(WebActionsException e){
            Report.reportFailure("No se pudo escoger ningún producto", e);
        }
    }

    public void seleccionarProductosCarrito(){
        int contador = START_VARIABLES;
        try {
            while (contador != NUMBER_PRODUCTS) {
                ProductsPage productsPage = new ProductsPage(webAction.getDriver());
                webAction.click(productsPage.randomProducts(webAction.getDriver()),TEN_SECONDS.getValue(), true);
                webAction.click(productsPage.getAddToCart(),TEN_SECONDS.getValue(), true);
                webAction.click(productsPage.getClosePage(),TEN_SECONDS.getValue(),true);
                webAction.click(productsPage.getReturnShooping(),TEN_SECONDS.getValue(),true);
                contador++;
            }
            ProductsPage productsPage = new ProductsPage(webAction.getDriver());
            webAction.click(productsPage.getCheckOut(),true);
        }catch(WebActionsException e){
            Report.reportFailure("No se pudo escoger ningún producto", e);
        }
    }
}
