package co.com.sofka.mystore.test.stepdefinition.setup;

import co.com.sofka.mystore.test.helpers.TestInfo;
import co.com.sofka.test.automationtools.selenium.Browser;
import co.com.sofka.test.utils.files.PropertiesFile;
import org.junit.BeforeClass;
import org.openqa.selenium.chrome.ChromeOptions;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Setup {

    protected TestInfo testInfo;

    protected Browser browser(){
        Browser browserConfiguration = new Browser();
        browserConfiguration.setBrowser(Browser.Browsers.CHROME);
        browserConfiguration.setIncognito(true);
        browserConfiguration.setMaximized(true);
        browserConfiguration.setAutoDriverDownload(true);

        browserConfiguration.setChromeOptions(chromeOptions());

        return browserConfiguration;
    }

    private ChromeOptions chromeOptions(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        return options;
    }

    public static void setupFile() {

        Path propertiesFolder = Paths.get(System.getProperty("user.dir"), "src/main/resources/properties/");
        PropertiesFile propertiesFile = new PropertiesFile("default", propertiesFolder);
        String ruta = System.getProperty("user.dir");

        propertiesFile.updateFieldValue("files.evidence", ruta+"/src/main/resources/evidence");
    }
}
