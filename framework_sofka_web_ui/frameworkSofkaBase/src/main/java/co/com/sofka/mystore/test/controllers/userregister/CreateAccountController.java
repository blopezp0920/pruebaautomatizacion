package co.com.sofka.mystore.test.controllers.userregister;

import co.com.sofka.mystore.test.models.Customer;
import co.com.sofka.mystore.test.page.accountpage.CreateAnAccountPage;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;

import static co.com.sofka.mystore.test.helpers.Seconds.TEN_SECONDS;
import static co.com.sofka.mystore.test.helpers.Seconds.TWO_SECONDS;

public class CreateAccountController {
    private WebAction webAction;
    private Customer customer;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void crearUnaCuenta(){
        try {
            CreateAnAccountPage createAnAccountPage = new CreateAnAccountPage(webAction.getDriver());

            webAction.sendText(
                    createAnAccountPage.getEmailAddress(),
                    customer.getEmail(),
                    TWO_SECONDS.getValue(),
                    true
            );
            webAction.click(createAnAccountPage.getCreateAnAccount(), TWO_SECONDS.getValue(), true);
            webAction.click(createAnAccountPage.getMr(), TEN_SECONDS.getValue(), true);
            webAction.sendText(createAnAccountPage.getFirstName(), customer.getFirstName(), true);
            webAction.sendText(createAnAccountPage.getLastName(), customer.getLastName(), true);
            webAction.sendText(createAnAccountPage.getPassword(), customer.getPassword(), true);
            webAction.selectByPartialText(createAnAccountPage.getDay(), customer.getDayBirth(), true);
            webAction.selectByValue(createAnAccountPage.getMonth(), customer.getMonthBirth(), true);
            webAction.selectByPartialText(createAnAccountPage.getYear(), customer.getYearBirth(), true);
            webAction.sendText(createAnAccountPage.getAddress1(), customer.getAddress(), true);
            webAction.sendText(createAnAccountPage.getCity(), customer.getCity(), true);
            webAction.selectByText(createAnAccountPage.getState(), customer.getState(), true);
            webAction.sendText(createAnAccountPage.getPostCode(), customer.getPostalCode(), true);
            webAction.sendText(createAnAccountPage.getPhoneMobile(), customer.getMobilePhone(), true);
            webAction.click(createAnAccountPage.getRegister(), true);
            webAction.click(createAnAccountPage.getHome(),TEN_SECONDS.getValue(),true);

        } catch (WebActionsException e) {
            Report.reportFailure("Ocurrió un error al llenar la información de usuario", e);
        }
    }
}
