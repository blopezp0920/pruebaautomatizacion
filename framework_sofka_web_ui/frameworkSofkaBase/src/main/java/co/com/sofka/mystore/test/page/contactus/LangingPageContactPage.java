package co.com.sofka.mystore.test.page.contactus;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LangingPageContactPage {

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"contact-link\"]/a")
    WebElement contactUs;

    public WebElement getContactUs(){return this.contactUs;}

    public LangingPageContactPage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }

}
