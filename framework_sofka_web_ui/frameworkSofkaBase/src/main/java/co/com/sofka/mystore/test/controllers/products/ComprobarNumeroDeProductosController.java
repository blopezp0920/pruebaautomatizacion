package co.com.sofka.mystore.test.controllers.products;

import co.com.sofka.mystore.test.page.products.ProductsPage;
import co.com.sofka.test.actions.WebAction;

public class ComprobarNumeroDeProductosController {

    private WebAction webAction;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public int comprobarNumeroDeProductos(){

            ProductsPage productsPage = new ProductsPage(webAction.getDriver());

        return productsPage.getSizeProducts(webAction.getDriver());
    }
}
