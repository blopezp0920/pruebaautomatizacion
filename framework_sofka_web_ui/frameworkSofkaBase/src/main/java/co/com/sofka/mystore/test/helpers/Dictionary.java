package co.com.sofka.mystore.test.helpers;

public class Dictionary {

    private Dictionary() {
    }

    public static final String STATE_BY_DEFAULT_FLORIDA = "Florida";
    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";

    public static final String SPANISH_CODE_LANGUAGE = "es";
    public static final String COUNTRY_CODE = "CO";

    public static final String EMAIL_DOMAIN = "@sofmail.com";

    public static final String APP_URL_PROPERTY = "app.url";

    private static final String PROPERTIES_FILE_BASE_PATH = "src/main/resources/properties/";
    public static final String SUBJECT_HEADING = "Customer service";
    public static final int START_VARIABLES = 0;
    public static final int NUMBER_PRODUCTS = 4;

    public static final String CONTACT_SUCCESS = "Your message has been successfully sent to our team.";
    public static final String MESSAGE_ERROR = "The message cannot be blank.";
    public static final String MESSAGE_ORDER_COMPLETE = "Your order on My Store is complete.";

    public static final String CONFIGURATION_PROPERTIES_FILE = System.getProperty("user.dir") +
            "/" +
            PROPERTIES_FILE_BASE_PATH + "configuration.properties";

}
