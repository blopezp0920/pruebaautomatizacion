package co.com.sofka.mystore.test.models;

public class ContactUsModel {


    private  String subjecHeading;

    private String email;
    private String orderReference;
    private String mensaje;

    public String getSubjectHeading() {
        return subjecHeading;
    }

    public void setSubjectHeading(String subjectModel) {
        this.subjecHeading = subjectModel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrderReference() {
        return orderReference;
    }

    public void setOrderReference(String orderReference) {
        this.orderReference = orderReference;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
