package co.com.sofka.mystore.test.page.products;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

public class ProductsPage {

    Random random = new Random();
    @CacheLookup
    @FindBy(css = "#add_to_cart > button > span")
    WebElement addToCart;

    @CacheLookup
    @FindBy(css = "#header_logo > a")
    WebElement returnShooping;

    @CacheLookup
    @FindBy(css = "#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a")
    WebElement checkOut;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"center_column\"]/p[2]/a[1]/span")
    WebElement procededToCheckOut;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"center_column\"]/form/p/button/span")
    WebElement procededToCheckOutAddress;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"form\"]/div/p[2]/label")
    WebElement termsOfService;

    @CacheLookup
    @FindBy(css = "#form > p > button > span")
    WebElement procededToCheckOutShipping;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"HOOK_PAYMENT\"]/div[1]/div/p/a")
    WebElement payBank;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"cart_navigation\"]/button/span")
    WebElement confirmOrder;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"center_column\"]/div/p/strong")
    WebElement orderComplete;

    @CacheLookup
    @FindBy(css = "#layer_cart > div.clearfix > div.layer_cart_product.col-xs-12.col-md-6 > span")
    WebElement closePage;

    @CacheLookup
    @FindBy(css = "#center_column > div.tab-content > #homefeatured li")
    WebElement listProducts;

    public WebElement getListProducts() {
        return listProducts;
    }

    public WebElement getReturnShooping() {
        return returnShooping;
    }

    public WebElement getCheckOut() {
        return checkOut;
    }

    public WebElement getAddToCart() {
        return addToCart;
    }

    public WebElement getClosePage() {
        return closePage;
    }

    public WebElement  randomProducts(WebDriver driver){
        int randomProduct;
        int maxProducts;
        List<WebElement> productElems = driver.findElements(By.cssSelector("#center_column > div.tab-content > #homefeatured li"));
         maxProducts = productElems.size();

         randomProduct= random.nextInt(maxProducts);
        return productElems.get(randomProduct);

    }

    public WebElement  randomDeleteProducts(WebDriver driver){
        int randomDeleteProducts;
        int maxIDeleteProducts;
        List<WebElement> productElems = driver.findElements(By.cssSelector("td div a i.icon-trash"));
        maxIDeleteProducts = productElems.size();

        randomDeleteProducts= random.nextInt(maxIDeleteProducts);
        return productElems.get(randomDeleteProducts);

    }

    public int getSizeProducts(WebDriver driver){

        List<WebElement> productElems = driver.findElements(By.cssSelector("td div a i.icon-trash"));
        return productElems.size();

    }
    public WebElement getProcededToCheckOut() {
        return procededToCheckOut;
    }

    public WebElement getProcededToCheckOutAddress() {
        return procededToCheckOutAddress;
    }

    public WebElement getTermsOfService() {
        return termsOfService;
    }

    public WebElement getProcededToCheckOutShipping() {
        return procededToCheckOutShipping;
    }

    public WebElement getPayBank() {
        return payBank;
    }

    public WebElement getConfirmOrder() {
        return confirmOrder;
    }

    public WebElement getOrderComplete() {
        return orderComplete;
    }

    public ProductsPage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
    }
}
