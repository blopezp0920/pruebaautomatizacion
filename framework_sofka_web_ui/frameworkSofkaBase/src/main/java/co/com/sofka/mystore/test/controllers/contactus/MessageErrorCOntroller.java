package co.com.sofka.mystore.test.controllers.contactus;

import co.com.sofka.mystore.test.page.contactus.ContactUsPage;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Report;

import static co.com.sofka.mystore.test.helpers.Seconds.FIVE_SECONDS;

public class MessageErrorCOntroller {

    private WebAction webAction;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public String obtenerMensajeDeError(){
        String message = "";
        try {
            ContactUsPage contactUsPage = new ContactUsPage(webAction.getDriver());
            message = webAction.getText(contactUsPage.getMessageError(),FIVE_SECONDS.getValue(),true);

        } catch (Exception e) {
            Report.reportFailure("Ocurrió un error al intentar validar los campos.", e);
        }
        return message;
    }
}
