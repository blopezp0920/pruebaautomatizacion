package co.com.sofka.mystore.test.runners;

import co.com.sofka.test.utils.files.PropertiesFile;
import org.junit.BeforeClass;

import java.nio.file.Path;
import java.nio.file.Paths;

public class TestRunner {

    @BeforeClass
    public static void setupFile() {

        Path propertiesFolder = Paths.get(System.getProperty("user.dir"), "src/main/resources/properties/");
        PropertiesFile propertiesFile = new PropertiesFile("default", propertiesFolder);
        String ruta = System.getProperty("user.dir");

        propertiesFile.updateFieldValue("files.evidence", ruta+"/src/main/resources/evidence");
    }


}
