package co.com.sofka.mystore.test.stepdefinition.compradeproductos;

import co.com.sofka.mystore.test.controllers.openwebpage.StartBrowserController;
import co.com.sofka.mystore.test.controllers.products.EliminarUnProductoController;
import co.com.sofka.mystore.test.controllers.products.ComprobarNumeroDeProductosController;
import co.com.sofka.mystore.test.controllers.products.ProdcutsSelectionController;
import co.com.sofka.mystore.test.controllers.userregister.CreateAccountController;
import co.com.sofka.mystore.test.controllers.userregister.LoginPageWebController;
import co.com.sofka.mystore.test.helpers.TestInfo;
import co.com.sofka.mystore.test.models.Customer;
import co.com.sofka.mystore.test.stepdefinition.setup.Setup;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Assert;
import co.com.sofka.test.evidence.reports.Report;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;

import static co.com.sofka.mystore.test.helpers.Dictionary.*;
import static co.com.sofka.mystore.test.helpers.Helper.generateCustomer;

public class EliminarUnProductoStepDefinition extends Setup {

    private WebAction webAction;
    private Customer customer;

    @Before
    public void setup(Scenario scenario){
        setupFile();
        testInfo = new TestInfo(scenario);
        webAction = new WebAction(testInfo.getFeatureName());
        customer = generateCustomer(SPANISH_CODE_LANGUAGE, COUNTRY_CODE, EMAIL_DOMAIN);
    }
    @Dado("que el usuario esté logueado y haya seleccionado los productos")
    public void queElUsuarioEsteLogueadoYHayaSeleccionadoLosProductos() {
        StartBrowserController startBrowserWebController = new StartBrowserController();
        startBrowserWebController.setBrowser(browser());
        startBrowserWebController.setWebAction(webAction);
        startBrowserWebController.setFeature(testInfo.getFeatureName());
        startBrowserWebController.abrirTiendaOnline();

        LoginPageWebController loginPageWebController = new LoginPageWebController();
        loginPageWebController.setWebAction(webAction);
        loginPageWebController.irHaciaLoginPage();

        CreateAccountController createAccountController = new CreateAccountController();
        createAccountController.setCustomer(customer);
        createAccountController.setWebAction(webAction);
        createAccountController.crearUnaCuenta();

        ProdcutsSelectionController prodcutsSelectionController = new ProdcutsSelectionController();
        prodcutsSelectionController.setWebAction(webAction);
        prodcutsSelectionController.seleccionarProductosCarrito();
    }

    @Cuando("el usuario ingresa al carrito de compras y elimina un producto")
    public void elUsuarioIngresaAlCarritoDeComprasYEliminaUnProducto() {
        EliminarUnProductoController eliminarUnProductoController = new EliminarUnProductoController();
        eliminarUnProductoController.setWebAction(webAction);
        eliminarUnProductoController.eliminarUnProducto();
    }

        @Entonces("entonces el producto desaparecerá del carrito de compras")
        public void entoncesElProductoDesapareceraDelCarritoDeCompras() {

        ComprobarNumeroDeProductosController comprobarNumeroDeProductosController = new ComprobarNumeroDeProductosController();
        comprobarNumeroDeProductosController.setWebAction(webAction);
        Assert.Hard
                .thatInteger(
                        comprobarNumeroDeProductosController.comprobarNumeroDeProductos()
                ).isLessThan(
                        NUMBER_PRODUCTS
                );
    }

    @After
    public void cerrarDriver(){

        if (webAction != null && webAction.getDriver() != null)
            webAction.closeBrowser();

        Report.reportInfo("***** HA FINALIZADO LA PRUEBA******"
                .concat(testInfo.getFeatureName())
                .concat("-")
                .concat(testInfo.getScenarioName()));
    }

}
