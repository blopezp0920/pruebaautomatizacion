package co.com.sofka.mystore.test.controllers.userregister;

import co.com.sofka.mystore.test.page.accountpage.LandingPage;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;

import static co.com.sofka.mystore.test.helpers.Seconds.TWO_SECONDS;

public class LoginPageWebController {
    private WebAction webAction;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public void irHaciaLoginPage(){
        try {
            LandingPage landingPage = new LandingPage(webAction.getDriver());
            webAction.click(landingPage.getSignIn(), TWO_SECONDS.getValue(), true);
        } catch (WebActionsException e) {
            Report.reportFailure("Ocurrió un error al intentar ir al login", e);
        }
    }
}
