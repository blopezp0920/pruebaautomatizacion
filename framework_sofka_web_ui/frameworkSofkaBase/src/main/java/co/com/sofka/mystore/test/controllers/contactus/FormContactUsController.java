package co.com.sofka.mystore.test.controllers.contactus;

import co.com.sofka.mystore.test.models.ContactUsModel;
import co.com.sofka.mystore.test.page.contactus.ContactUsPage;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;

import static co.com.sofka.mystore.test.helpers.Seconds.FIVE_SECONDS;

public class FormContactUsController {

    private WebAction webAction;
    private ContactUsModel contactUsModel;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public void setContactUsModel(ContactUsModel contactUsModel) {
        this.contactUsModel = contactUsModel;
    }

    public void llenarCampos(){
        try{
            ContactUsPage contactUsPage = new ContactUsPage(webAction.getDriver());
            webAction.selectByText(contactUsPage.getSubjectHeading(),contactUsModel.getSubjectHeading(),FIVE_SECONDS.getValue(),true);
            webAction.sendText(contactUsPage.getEmail(),contactUsModel.getEmail(),FIVE_SECONDS.getValue(),true);
            webAction.sendText(contactUsPage.getOrderReference(),contactUsModel.getOrderReference(),FIVE_SECONDS.getValue(),true);
            webAction.sendText(contactUsPage.getMessage(),contactUsModel.getMensaje(),FIVE_SECONDS.getValue(),true);
            webAction.clickJS(contactUsPage.getSend(),true);
        } catch (WebActionsException e) {
            Report.reportFailure("Ocurrió un error al llenar la información de contacto", e);
        }
    }
    public void llenarCamposSinMessage(){
        try{
            ContactUsPage contactUsPage = new ContactUsPage(webAction.getDriver());
            webAction.selectByText(contactUsPage.getSubjectHeading(),contactUsModel.getSubjectHeading(),true);
            webAction.sendText(contactUsPage.getEmail(),contactUsModel.getEmail(),true);
            webAction.sendText(contactUsPage.getOrderReference(),contactUsModel.getOrderReference(),true);
            webAction.clickJS(contactUsPage.getSend(),true);
        } catch (WebActionsException e) {
            Report.reportFailure("Ocurrió un error al llenar la información de contacto", e);
        }
    }

}
