package co.com.sofka.mystore.test.controllers.products;

import co.com.sofka.mystore.test.page.products.ProductsPage;
import co.com.sofka.test.actions.WebAction;
import co.com.sofka.test.evidence.reports.Report;
import co.com.sofka.test.exceptions.WebActionsException;
import org.openqa.selenium.WebElement;

import static co.com.sofka.mystore.test.helpers.Seconds.FIVE_SECONDS;

public class EliminarUnProductoController {

    private WebAction webAction;

    public void setWebAction(WebAction webAction) {
        this.webAction = webAction;
    }

    public void eliminarUnProducto(){
        WebElement webElement;
        try{
            ProductsPage productsPage = new ProductsPage(webAction.getDriver());
            webElement = productsPage.randomDeleteProducts(webAction.getDriver());
            webAction.click(webElement,FIVE_SECONDS.getValue(),true);
            webAction.waitForInvisibility(webElement,FIVE_SECONDS.getValue(),true);

        } catch (WebActionsException e) {
            Report.reportFailure("No se pudo eliminar el producto", e);
        }

    }
}
