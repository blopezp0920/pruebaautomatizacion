package co.com.sofka.utils;

public enum VariableStart {

    VARIABLE_START_INDEX_PRODUCTS(0),
    VARIABLE_END_PRODUCTS(1),
    VARIABLE_START_COUNTER(0),
    VARIABLE_START_PRODUCTS_COUNTER(0);

    private final int value;

    VariableStart(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
