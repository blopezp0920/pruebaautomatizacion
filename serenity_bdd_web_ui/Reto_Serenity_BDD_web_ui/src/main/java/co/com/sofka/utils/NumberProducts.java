package co.com.sofka.utils;

public enum NumberProducts {

    ONE_PRODUCTS(1),
    FOUR_PPRODUCTS(4);

    private final int value;

    NumberProducts(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
