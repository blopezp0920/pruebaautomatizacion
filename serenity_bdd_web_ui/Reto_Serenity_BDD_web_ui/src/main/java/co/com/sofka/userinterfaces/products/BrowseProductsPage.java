package co.com.sofka.userinterfaces.products;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class BrowseProductsPage extends PageObject {


    public static final Target PRODUCTS = Target
            .the("Products")
            .locatedBy("#main > div > div.col.large-9 > div > " +
                    "div.products.row.row-small.large-columns-3.medium-columns-3.small-columns-2.has-shadow.row-box-shadow-1.row-box-shadow-4-hover.equalize-box > " +
                    "div.product-small.col.has-hover.product.type-product");

    public static final Target BOX_PRODUCTS = Target
            .the("Box products")
            .located(By.xpath("//*[@id=\"main\"]/div"));
    public static final Target CHOOSE_SELECTION = Target
            .the("Choose selection")
            .locatedBy("div.product-main > div > div.product-info.summary.col-fit.col.entry-summary.product-summary  select");

    public static final Target IMAGE = Target
            .the("Image")
            .located(By.cssSelector("div > div.product-main > div > div.product-gallery.large-7.col " +
                    "> div.product-images.relative.mb-half.has-hover.woocommerce-product-gallery.woocommerce-product-gallery--with-" +
                    "images.woocommerce-product-gallery--columns-4.images > figure > div > div > div.woocommerce-product-gallery__image.slide." +
                    "first.is-selected > a > img"));

    public static final Target SUBMIT = Target
            .the("Submit")
            .located(By.cssSelector("div > div.product-main > div > div.product-info.summary.col-fit.col.entry-summary.product-summary " +
                    "> form > div.cart.bundle_data.bundle_data > div > div.bundle_button > button"));

    public static final Target ERROR_MESSAGE = Target
            .the("No products found")
            .located(By.cssSelector("#main > div.cart-container.container.page-wrapper.page-checkout > div > div.woocommerce-notices-wrapper > ul > li > div"));

    public static final Target OFFERS = Target
            .the("Offers")
            .located(By.cssSelector("#mega-menu-item-198556 > a"));

    public static final Target SHOPPING_CAR = Target
            .the("Shopping car")
            .located(By.cssSelector("#masthead > div > div.flex-col.hide-for-medium.flex-right > ul > li.cart-item.has-icon.has-dropdown > div > a"));

    public static final Target EMPTY_CART = Target
            .the("Empty cart")
            .located(By.cssSelector("#main > div.cart-container.container.page-wrapper.page-checkout > " +
                    "div > div.woocommerce.row.row-large.row-divided > div.col.large-7.pb-0.cart-auto-refresh > form > div > a"));

    public static final Target CHECK_OUT = Target
            .the("Checkout")
            .located(By.xpath("//*[@id=\"main\"]/div[2]/div/div[2]/div[2]/div/div[1]/div/a"));

    public static final Target IDENTITY_CARD = Target
            .the("Indentity card")
            .located(By.xpath("//*[@id=\"billing_myfield12\"]"));

    public static final Target EMAIL = Target
            .the("Email")
            .located(By.xpath("//*[@id=\"billing_email\"]"));

    public static final Target NAME = Target
            .the("Name")
            .located(By.xpath("//*[@id=\"billing_first_name\"]"));

    public static final Target LAST_NAME = Target
            .the("Last name")
            .located(By.xpath("//*[@id=\"billing_last_name\"]"));

    public static final Target DEPARTMENT = Target
            .the("DEPARTMEN")
            .located(By.xpath("//*[@id=\"billing_state_field\"]/span/span/span[1]/span"));

    public static final Target CITY = Target
            .the("City")
            .located(By.xpath("//*[@id=\"billing_city_field\"]/span/span[1]/span"));

    public static final Target ADDRESS = Target
            .the("Address")
            .located(By.xpath("//*[@id=\"billing_address_1\"]"));

    public static final Target APARTMENT = Target
            .the("Apartment")
            .located(By.xpath("//*[@id=\"billing_address_2\"]"));

    public static final Target PHONE = Target
            .the("Phone")
            .located(By.xpath("//*[@id=\"billing_phone\"]"));

    public static final Target TERM_AND_CONDITIONS = Target
            .the("Term and conditions")
            .located(By.xpath("/html/body/div[1]/main/div[2]/div/form[2]/div/div[2]/div/div/div[1]/div/div/div/p/label/span[1]"));

    public static final Target SUBMIT_ORDER = Target
            .the("Submit ordeer")
            .located(By.xpath("//*[@id=\"place_order\"]"));

    public static final Target ORDER_COMPLETED = Target
            .the("Order completed")
            .located(By.xpath("//*[@id=\"main\"]/div[2]/div/div/div[2]/div"));

    public static final Target SEARCH_DEPARTMENT = Target
            .the("Search department")
            .locatedBy("#select2-billing_state-results li");

    public static final Target SEARCH_CITY = Target
            .the("Search department")
            .locatedBy("#select2-billing_city-results li");

    public static final Target MESSAGE_EMPTY_CART = Target
            .the("Message empty cart")
            .located(By.xpath("//*[@id=\"main\"]/div[2]/div/div/p[1]"));

    public static final Target MESSAGE_EMPTY_FLAVOR = Target
            .the("Message empty flavor")
            .locatedBy("div > div.product-main > div > div.product-info.summary.col-fit.col.entry-summary.product-summary" +
                    " > form > div.bundled_product.bundled_product_summary.product > div.details > div > div > div > div > p");

    public static final Target SEARCH_CITY_BOX = Target
            .the("Search city box")
            .located(By.xpath("/html/body/span/span/span[1]/input"));
}


