package co.com.sofka.tasks.products;

import co.com.sofka.models.ClientModel;
import co.com.sofka.utils.Seconds;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import static co.com.sofka.userinterfaces.products.BrowseProductsPage.*;

public class FormClient implements Task {


    private ClientModel clientModel;

    public FormClient usingInformationCLient(ClientModel clientModel){
        this.clientModel = clientModel;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the(IDENTITY_CARD, WebElementStateMatchers.isClickable())
                        .forNoMoreThan(Seconds.TEN_SECONDS.getValue())
                        .seconds(),
                Scroll.to(IDENTITY_CARD),
                Clear.field(IDENTITY_CARD),
                Enter.theValue(clientModel.getIdentity()).into(IDENTITY_CARD),

                Scroll.to(EMAIL),
                Clear.field(EMAIL),
                Enter.theValue(clientModel.getEmail()).into(EMAIL),

                Scroll.to(NAME),
                Clear.field(NAME),
                Enter.theValue(clientModel.getFirstName()).into(NAME),

                Scroll.to(LAST_NAME),
                Clear.field(LAST_NAME),
                Enter.theValue(clientModel.getLastName()).into(LAST_NAME),

                Scroll.to(DEPARTMENT),
                MoveMouse.to(DEPARTMENT),
                Hit.the(Keys.ARROW_DOWN).keyIn(DEPARTMENT),
                Hit.the(Keys.ENTER).keyIn(SEARCH_DEPARTMENT),

                WaitUntil.the(CITY, WebElementStateMatchers.isClickable())
                        .forNoMoreThan(Seconds.TEN_SECONDS.getValue())
                        .seconds(),

                Scroll.to(CITY),
                MoveMouse.to(CITY),
                Hit.the(Keys.ARROW_DOWN).keyIn(CITY),

                Hit.the(Keys.DOWN).keyIn(SEARCH_CITY),
                WaitUntil.the(SEARCH_CITY, WebElementStateMatchers.isClickable())
                        .forNoMoreThan(Seconds.TEN_SECONDS.getValue())
                        .seconds(),

                Hit.the(Keys.ENTER).into(SEARCH_CITY),

                WaitUntil.the(CITY, WebElementStateMatchers.isClickable())
                        .forNoMoreThan(Seconds.TEN_SECONDS.getValue())
                        .seconds(),

                Scroll.to(ADDRESS),
                Clear.field(ADDRESS),
                Enter.theValue(clientModel.getAddress()).into(ADDRESS),

                WaitUntil.the(APARTMENT, WebElementStateMatchers.isClickable())
                        .forNoMoreThan(Seconds.TEN_SECONDS.getValue())
                        .seconds(),

                Scroll.to(APARTMENT),
                Clear.field(APARTMENT),
                Enter.theValue(clientModel.getAparment()).into(APARTMENT),

                WaitUntil.the(PHONE, WebElementStateMatchers.isVisible())
                        .forNoMoreThan(Seconds.TEN_SECONDS.getValue())
                        .seconds(),

                Scroll.to(PHONE),
                Clear.field(PHONE),
                Enter.theValue(clientModel.getPhone()).into(PHONE),

                Scroll.to(TERM_AND_CONDITIONS),
                Click.on(TERM_AND_CONDITIONS),

                Scroll.to(SUBMIT_ORDER),
                Click.on(SUBMIT_ORDER),
                WaitUntil.the(ORDER_COMPLETED, WebElementStateMatchers.isVisible())
                        .forNoMoreThan(Seconds.TWENTY_SECONDS.getValue())
                        .seconds()
        );

    }
    public static FormClient formClient(){return new FormClient();}

}
