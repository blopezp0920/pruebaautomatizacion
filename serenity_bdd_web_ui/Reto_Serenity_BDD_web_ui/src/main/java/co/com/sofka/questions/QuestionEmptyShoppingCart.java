package co.com.sofka.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.Objects;

import static co.com.sofka.userinterfaces.products.BrowseProductsPage.MESSAGE_EMPTY_CART;

public class QuestionEmptyShoppingCart implements Question<Boolean> {

    private String yourCartIsEmpty;

    public QuestionEmptyShoppingCart  yourCartIsEmpty(String yourCartIsEmpty ){
        this.yourCartIsEmpty = yourCartIsEmpty;
        return this;
    }
    @Override
    public Boolean answeredBy(Actor actor) {
        return (Objects.equals(MESSAGE_EMPTY_CART.resolveFor(actor).getText(), yourCartIsEmpty));
    }


    public QuestionEmptyShoppingCart is(){
        return this;
    }


    public static QuestionEmptyShoppingCart questionEmptyShoppingCart(){return new QuestionEmptyShoppingCart();}
}
