package co.com.sofka.utils;

public enum NombreActores {

    NOMBRE_ACTOR_PEDIDO_EXITOSO("Bryan");

    private final String value;

    public String getValue() {
        return value;
    }

    NombreActores(String value) {
        this.value = value;
    }

}
