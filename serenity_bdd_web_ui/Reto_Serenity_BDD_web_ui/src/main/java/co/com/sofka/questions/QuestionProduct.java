package co.com.sofka.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterfaces.products.BrowseProductsPage.ORDER_COMPLETED;

public class QuestionProduct implements Question<Boolean> {

    private String thankYouForTheOrder;

    public QuestionProduct messageOrder(String thankYouForTheOrder ){
        this.thankYouForTheOrder = thankYouForTheOrder;
        return this;
    }

    public QuestionProduct is(){
        return this;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return (ORDER_COMPLETED.resolveFor(actor).isVisible());
    }

    public static QuestionProduct questionProduct(){
        return new QuestionProduct();
    }

}
