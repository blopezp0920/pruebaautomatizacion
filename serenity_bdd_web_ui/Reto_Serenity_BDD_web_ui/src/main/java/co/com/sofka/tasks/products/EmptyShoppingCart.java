package co.com.sofka.tasks.products;

import co.com.sofka.utils.Seconds;
import co.com.sofka.utils.VariableStart;
import net.serenitybdd.core.pages.ListOfWebElementFacades;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import java.util.Random;

import static co.com.sofka.userinterfaces.products.BrowseProductsPage.*;
import static co.com.sofka.userinterfaces.products.BrowseProductsPage.SUBMIT;

public class EmptyShoppingCart implements Task {


    private final Random rand = new Random();
    private int productsIndex = VariableStart.VARIABLE_START_INDEX_PRODUCTS.getValue();

    @Override
    public <T extends Actor> void performAs(T actor) {


            checkOut(actor);
            emptyCart(actor);

    }

    public <T extends Actor> void emptyCart(T actor) {
        actor.attemptsTo(
                MoveMouse.to(EMPTY_CART),
                Click.on(EMPTY_CART),
                WaitUntil.the(MESSAGE_EMPTY_CART, WebElementStateMatchers.isVisible())
                        .forNoMoreThan(Seconds.TWENTY_SECONDS.getValue())
                        .seconds()

        );
    }
    public <T extends Actor> void checkOut(T actor) {
        int numberOfProductsCounter = VariableStart.VARIABLE_START_PRODUCTS_COUNTER.getValue();
        while (VariableStart.VARIABLE_END_PRODUCTS.getValue() != numberOfProductsCounter){

            randomProductsOffer(actor);
            addToCar(actor);
            numberOfProductsCounter++;
            selectionProductsOffer(actor);
        }
            goToShoppingCar(actor);

    }
    public <T extends Actor> void goToShoppingCar(T actor) {
        actor.attemptsTo(
                MoveMouse.to(SHOPPING_CAR),
                Click.on(SHOPPING_CAR)
        );
    }
    public <T extends Actor> void selectionProductsOffer(T actor) {
        actor.attemptsTo(
                WaitUntil.the(IMAGE, WebElementStateMatchers.isVisible())
                        .forNoMoreThan(Seconds.TEN_SECONDS.getValue())
                        .seconds(),
                MoveMouse.to(OFFERS),
                Click.on(OFFERS)
        );
    }
    public <T extends Actor> void randomProductsOffer(T actor) {
        actor.attemptsTo(
                WaitUntil.the(BOX_PRODUCTS, WebElementStateMatchers.isVisible())
                        .forNoMoreThan(Seconds.TEN_SECONDS.getValue())
                        .seconds(),
                Scroll.to(PRODUCTS.resolveAllFor(actor).get(randomProducts(PRODUCTS.resolveAllFor(actor)))),
                Click.on(PRODUCTS.resolveAllFor(actor).get(productsIndex)),

                WaitUntil.the(IMAGE, WebElementStateMatchers.isVisible())
                        .forNoMoreThan(Seconds.TEN_SECONDS.getValue())
                        .seconds()
        );
    }

    public <T extends Actor> void addToCar(T actor) {
        int indexCount = VariableStart.VARIABLE_START_COUNTER.getValue();
        if (!MESSAGE_EMPTY_FLAVOR.resolveFor(actor).isVisible()) {
            while (CHOOSE_SELECTION.resolveAllFor(actor).size() != indexCount) {

                actor.attemptsTo(
                        MoveMouse.to(CHOOSE_SELECTION.resolveAllFor(actor).get(indexCount)),
                        Hit.the(Keys.ARROW_DOWN).keyIn(CHOOSE_SELECTION.resolveAllFor(actor).get(indexCount))
                );
                indexCount++;
            }
        }
        if (SUBMIT.isVisibleFor(actor)){
            actor.attemptsTo(
                    Scroll.to(SUBMIT),
                    Click.on(SUBMIT)
            );
        }
    }

    protected int randomProducts (ListOfWebElementFacades listOfWebElementFacades) {
        productsIndex = this.rand.nextInt(listOfWebElementFacades.size());
        return productsIndex;
    }

    public static EmptyShoppingCart emptyShoppingCart(){return new EmptyShoppingCart();}


}
