#language: es

Característica: Vaciar el carrito de compras
  Como cliente
  necesito vaciar el carrito cuando tengas productos agregado al carrito de compras
  para eliminar los productos que no vaya a comprar

  Escenario: Vaciado de carritos de compras exitosa
    Dado que el usuario se encuentre en el apartado de ofertas
    Cuando el usuario agrega productos al carrito de compras y luego vacía el carrito
    Entonces el sistema mostrará un mensaje de que el carrito está vacío
