#language: es

Característica: Realizar un pedido de productos
  Como cliente
  necesito seleccionar los productos a comprar
  para realizar un pedido de los mismos

  Escenario: Realizar un pedido de manera exitosa
    Dado que el cliente esté en la página visualizando los productos
    Cuando el cliente busca y selecciona los productos
    Y el cliente registra la información del cliente y valida
    Entonces el sistema lo redirigirá al apartado de gracias por su compra
