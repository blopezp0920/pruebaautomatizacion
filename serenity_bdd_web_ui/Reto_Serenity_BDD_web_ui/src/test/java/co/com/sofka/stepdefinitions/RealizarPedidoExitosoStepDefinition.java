package co.com.sofka.stepdefinitions;

import co.com.sofka.models.ClientModel;
import co.com.sofka.setup.ui.Setup;

import static co.com.sofka.questions.QuestionProduct.questionProduct;
import static co.com.sofka.tasks.landingpage.OpenLandingPage.openLandingPage;

import co.com.sofka.utils.NombreActores;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;

import static co.com.sofka.tasks.products.BrowseProducts.browseProducts;
import static co.com.sofka.tasks.products.FormClient.formClient;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class RealizarPedidoExitosoStepDefinition extends Setup {

    private final Faker faker = new Faker();
    @Dado("que el cliente esté en la página visualizando los productos")
    public void queElClienteEsteEnLaPaginaVisualizandoLosProductos() {
        actorSetupTheBrowser(NombreActores.NOMBRE_ACTOR_PEDIDO_EXITOSO.getValue());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );
    }
    @Cuando("el cliente busca y selecciona los productos")
    public void elClienteBuscaYSeleccionaLosProductos() {
        theActorInTheSpotlight().attemptsTo(
                browseProducts()
        );

    }
    @Y("el cliente registra la información del cliente y valida")
    public void elClienteRegistraLaInformacionDelClienteYValida() {

        theActorInTheSpotlight().attemptsTo(
               formClient()
                       .usingInformationCLient(FormClient())
        );

    }
    @Entonces("el sistema lo redirigirá al apartado de gracias por su compra")
    public void elSistemaLoRedirigiraAlApartadoDeGraciasPorSuCompra() {

        theActorInTheSpotlight().should(
                seeThat(
                        questionProduct()
                                .messageOrder("Gracias. Tu pedido ha sido recibido.")
                                .is(), equalTo(true)
                )
        );

    }

    private ClientModel FormClient(){
        ClientModel clientModel = new ClientModel();
        clientModel.setIdentity(faker.idNumber().valid().replaceAll("\\D+",""));
        clientModel.setEmail(faker.internet().emailAddress());
        clientModel.setFirstName(faker.funnyName().name());
        clientModel.setLastName(faker.name().lastName());
        clientModel.setAparment(faker.address().fullAddress());
        clientModel.setAddress(faker.friends().location());
        clientModel.setPhone(faker.phoneNumber().cellPhone().replaceAll("\\D+",""));
        return clientModel;
    }
}
