package co.com.sofka.stepdefinitions;

import co.com.sofka.setup.ui.Setup;
import co.com.sofka.utils.NombreActores;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.questions.QuestionEmptyShoppingCart.questionEmptyShoppingCart;
import static co.com.sofka.tasks.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.tasks.products.EmptyShoppingCart.emptyShoppingCart;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class VaciarCarritoDeComprasStepDefinition extends Setup {

    @Dado("que el usuario se encuentre en el apartado de ofertas")
    public void queElUsuarioSeEncuentreEnElApartadoDeOfertas() {
        actorSetupTheBrowser(NombreActores.NOMBRE_ACTOR_PEDIDO_EXITOSO.getValue());
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage()
        );

    }
    @Cuando("el usuario agrega productos al carrito de compras y luego vacía el carrito")
    public void elUsuarioAgregaProductosAlCarritoDeComprasYLuegoVaciaElCarrito() {

        theActorInTheSpotlight().attemptsTo(
                emptyShoppingCart()
        );
    }
    @Entonces("el sistema mostrará un mensaje de que el carrito está vacío")
    public void elSistemaMostraraUnMensajeDeQueElCarritoEstaVacio() {

            theActorInTheSpotlight().should(
                    seeThat(
                            questionEmptyShoppingCart()
                                    .yourCartIsEmpty("Tu carrito está vacío.")
                                    .is(),equalTo(true)
                    )
            );
    }
}
