package co.com.sofka.pasoapaso.formularioregistro;

import co.com.sofka.configuracion.SetupWebUI;
import co.com.sofka.modelo.formulariosSitio7.ModeloFormularioContacto;
import co.com.sofka.modelo.formulariosSitio7.ModeloFormularioRegistro;
import co.com.sofka.page.comun.page.CasosFormularioRegistro;
import co.com.sofka.page.comun.page.CasosFormularioRegistroContacto;
import co.com.sofka.util.MensajesDeError;
import co.com.sofka.util.Opciones;
import co.com.sofka.util.PoliticasPublicas;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

public class PasoAPasoFormularioRegistroCucumberTest extends SetupWebUI {

    private static final Logger LOGGER = Logger.getLogger(PasoAPasoFormularioRegistroCucumberTest.class);
    private final Faker faker = new Faker();
    CasosFormularioRegistro formularioRegistro;
    CasosFormularioRegistroContacto casosFormularioRegistroContacto;

    @Dado("que el usuario se encuentra en la página web en el apartado registro")
    public void queElUsuarioSeEncuentraEnLaPaginaWebEnElApartadoRegistro() {
        try{
            setUpLog4j2();
            setUpWebDriver();
        } catch (Exception e){
            quiteDriver();
            Assertions.fail(e.getMessage(), e);
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Cuando("el usuario suministra los datos menos el nombre y confirma")
    public void elUsuarioSuministraLosDatosMenosElNombreYConfirma() {
        try{
            formularioRegistro = new CasosFormularioRegistro(driver, formularioRegistroSinNombre());
            formularioRegistro.camposFormularioRegistroUsuario(Opciones.OPCION_NOMBRE.getValue());
        } catch (Exception e){
            quiteDriver();
            Assertions.fail(e.getMessage(), e);
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el sistema mostrará un mensaje de error indicando que el nombre no ha sido suministrado.")
    public void elSistemaMostraraUnMensajeDeErrorIndicandoQueElNombreNoHaSidoSuministrado() {
        Assertions.assertEquals(MensajesDeError.MENSAJES_DE_ERROR_NOMBRE.getValue(), formularioRegistro.errorNombre());
        quiteDriver();
    }

    @Cuando("el usuario suministra los datos pero no ingresó un las contraseñas iguales")
    public void elUsuarioSuministraLosDatosPeroNoIngresoUnLasContraseniasIguales() {
        try{

            formularioRegistro = new CasosFormularioRegistro(driver, formularioRegistroSinConfirmarContrasenia());
            formularioRegistro.camposFormularioRegistroUsuario(Opciones.OPCION_CONTRASENIA.getValue());
        } catch (Exception e){
            quiteDriver();
            Assertions.fail(e.getMessage(), e);
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el sistema mostrará un mensaje de error indicando que las contraseñas son erroneas.")
    public void elSistemaMostraraUnMensajeDeErrorIndicandoQueLasContraseniaasSonErroneas() {

        Assertions.assertEquals(MensajesDeError.MENSAJES_DE_ERROR_CLAVES.getValue(), formularioRegistro.errorCampoSinIngresar());
        quiteDriver();
    }

    //Registro Contacto
    @Dado("que el usuario se encuentra en la página web en el apartado de contacto")
    public void queElUsuarioSeEncuentraEnLaPaginaWebEnElApartadoDeContacto() {
        try{
            setUpLog4j2();
            setUpWebDriver();
        } catch (Exception e){
            quiteDriver();
            Assertions.fail(e.getMessage(), e);
            LOGGER.error(e.getMessage(), e);
        }
    }
    @Cuando("el usuario suministra los datos pero no el asunto y confirma")
    public void elUsuarioSuministraLosDatosPeroNoElAsuntoYConfirma() {

        try{
            casosFormularioRegistroContacto = new CasosFormularioRegistroContacto(driver, formularioRegistroContactoSinAsunto());
            casosFormularioRegistroContacto.camposFormularioContacto(Opciones.OPCION_ASUNTO.getValue());
        } catch (Exception e){
            quiteDriver();
            Assertions.fail(e.getMessage(), e);
            LOGGER.error(e.getMessage(), e);
        }
    }
    @Entonces("el sistema deberá mostrar un error asociado al campo asunto faltante.")
    public void elSistemaDeberaMostrarUnErrorAsociadoAlCampoAsuntoFaltante() {

        Assertions.assertEquals(MensajesDeError.MENSAJES_DE_ERROR_ASUNTO.getValue(),casosFormularioRegistroContacto.errorAsunto());
        quiteDriver();
    }

    @Cuando("el usuario suministra los datos pero no ingresó el télefono")
    public void elUsuarioSuministraLosDatosPeroNoIngresoElTelefono() {
        try{
            casosFormularioRegistroContacto = new CasosFormularioRegistroContacto(driver, formularioRegistroContactoSinTelefono());
            casosFormularioRegistroContacto.camposFormularioContacto(Opciones.OPCION_TELEFONO.getValue());
        } catch (Exception e){
            quiteDriver();
            Assertions.fail(e.getMessage(), e);
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el sistema mostrará un mensaje de error indicando que falta suministrar la ifnormación del campo télefono")
    public void elSistemaMostraraUnMensajeDeErrorIndicandoQueFaltaSuministrarLaIfnormacionDelCampoTelefono() {
        Assertions.assertEquals(MensajesDeError.MENSAJES_DE_ERROR_TELEFONO.getValue(),casosFormularioRegistroContacto.errorTelefono());
        quiteDriver();
    }

    private ModeloFormularioRegistro formularioRegistroSinNombre(){
        ModeloFormularioRegistro modeloFormularioRegistro = new ModeloFormularioRegistro();
        modeloFormularioRegistro.setNombre("");
        modeloFormularioRegistro.setApellido(faker.name().lastName());
        modeloFormularioRegistro.setSitioWeb(faker.company().url());
        modeloFormularioRegistro.setEmail(faker.internet().emailAddress());
        modeloFormularioRegistro.setClave("Hunter0920..");
        modeloFormularioRegistro.setConfirmarClave("Hunter0920");
        modeloFormularioRegistro.setPoliticasPublicas(PoliticasPublicas.POLITCIAS_PUBLICAS);
        return modeloFormularioRegistro;
    }

    private ModeloFormularioRegistro formularioRegistroSinConfirmarContrasenia(){
        ModeloFormularioRegistro modeloFormularioRegistro = new ModeloFormularioRegistro();
        modeloFormularioRegistro.setNombre(faker.name().firstName());
        modeloFormularioRegistro.setApellido(faker.name().lastName());
        modeloFormularioRegistro.setSitioWeb(faker.company().url());
        modeloFormularioRegistro.setEmail(faker.internet().emailAddress());
        modeloFormularioRegistro.setClave(faker.internet().password());
        modeloFormularioRegistro.setConfirmarClave("");
        modeloFormularioRegistro.setPoliticasPublicas(PoliticasPublicas.POLITCIAS_PUBLICAS);
        return modeloFormularioRegistro;
    }

    private ModeloFormularioContacto formularioRegistroContactoSinAsunto(){
        ModeloFormularioContacto modeloFormularioContacto = new ModeloFormularioContacto();
        modeloFormularioContacto.setNombreYApellido(faker.name().fullName());
        modeloFormularioContacto.setEmail(faker.internet().emailAddress());
        modeloFormularioContacto.setAsunto("");
        modeloFormularioContacto.setTelefono(faker.phoneNumber().cellPhone());
        modeloFormularioContacto.setEmpresa(faker.company().name());
        modeloFormularioContacto.setMensaje(faker.medical().medicineName());
        return modeloFormularioContacto;
    }

    private ModeloFormularioContacto formularioRegistroContactoSinTelefono(){
        ModeloFormularioContacto modeloFormularioContacto = new ModeloFormularioContacto();
        modeloFormularioContacto.setNombreYApellido(faker.name().fullName());
        modeloFormularioContacto.setEmail(faker.internet().emailAddress());
        modeloFormularioContacto.setAsunto(faker.internet().macAddress());
        modeloFormularioContacto.setTelefono("");
        modeloFormularioContacto.setEmpresa(faker.company().name());
        modeloFormularioContacto.setMensaje(faker.medical().medicineName());
        return modeloFormularioContacto;
    }

}
