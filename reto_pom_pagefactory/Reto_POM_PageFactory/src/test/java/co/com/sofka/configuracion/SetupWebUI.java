package co.com.sofka.configuracion;

import co.com.sofka.util.SistemaOperativo;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Objects;

import static co.com.sofka.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static com.google.common.base.StandardSystemProperty.USER_DIR;

public class SetupWebUI {

    private static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";
    String WEBDRIVER_CHROME_DRIVER_PATH = "";
    private static final String DEMO_QA_URL = "http://institucional7.sublimesolutions.com/";
    protected WebDriver driver;

    protected void setUpWebDriver(){
        if (Objects.equals(SistemaOperativo.SISTEMA_OPERATIVO_LINUX.getValue(), SistemaOperativo.SITEMA_OPERATIVO.getValue())){
            WEBDRIVER_CHROME_DRIVER_PATH = "src/test/resources/webdriver/linux/chrome/chromedriver";
        }else {
            WEBDRIVER_CHROME_DRIVER_PATH = "src/test/resources/webdriver/widows/chrome/chromedriver.exe";
        }
        System.setProperty(WEBDRIVER_CHROME_DRIVER, WEBDRIVER_CHROME_DRIVER_PATH);
        generalSetUp();
    }

    private void generalSetUp(){

        driver = new ChromeDriver();
        driver.get(DEMO_QA_URL);
        driver.manage().window().maximize();
    }

    protected void setUpLog4j2(){
        PropertyConfigurator.configure(USER_DIR.value() + LOG4J_PROPERTIES_FILE_PATH.getValue());
    }
    protected void quiteDriver(){
        driver.quit();
    }
}
