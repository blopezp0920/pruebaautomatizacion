package co.com.sofka.pasoapaso.Login;

import co.com.sofka.configuracion.SetupWebUI;
import co.com.sofka.modelo.formulariosSitio7.ModeloFormularioLogin;
import co.com.sofka.page.comun.page.CasosFormularioLogin;
import co.com.sofka.pasoapaso.formularioregistro.PasoAPasoFormularioRegistroCucumberTest;
import co.com.sofka.util.MensajesDeError;
import co.com.sofka.util.Opciones;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;

public class PasoAPasoFormularioLoginCucumberTest extends SetupWebUI {

    private static final Logger LOGGER = Logger.getLogger(PasoAPasoFormularioRegistroCucumberTest.class);
    private final Faker faker = new Faker();
    CasosFormularioLogin casosFormularioLogin;

    @Dado("que el usuario se encuentra en la página web en el apartado ingresar")
    public void queElUsuarioSeEncuentraEnLaPaginaWebEnElApartadoIngresar() {
        try {
            setUpLog4j2();
            setUpWebDriver();
        } catch (Exception e) {
            quiteDriver();
            Assertions.fail(e.getMessage(), e);
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Cuando("El usuario ingresa los campos requeridos pero no el email")
    public void elUsuarioIngresaLosCamposRequeridosPeroNoElEmail() {

        try {
            casosFormularioLogin = new CasosFormularioLogin(driver, formualarioLogin());
            casosFormularioLogin.camposFormularioLogin(Opciones.OPCION_EMAIL.getValue());
        } catch (Exception e) {
            quiteDriver();
            Assertions.fail(e.getMessage(), e);
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Entonces("el sistema mostrará un mensaje de error inicando que falta ingresar el usuario y la contraseña")
    public void elSistemaMostraraUnMensajeDeErrorInicandoQueFaltaIngresarElUsuarioYLaContrasenia() {

        Assertions.assertEquals(casosFormularioLogin.errorEmail(), MensajesDeError.MENSAJE_DE_ERROR_EMAIL_CLAVE.getValue());
        quiteDriver();
    }

    @Cuando("El usuario ingresa los campos requeridos y valida")
    public void elUsuarioIngresaLosCamposRequeridosYValida() {
        try {
            casosFormularioLogin = new CasosFormularioLogin(driver, formularioLoginSinContrasenia());
            casosFormularioLogin.camposFormularioLogin(Opciones.OPCION_USUARIO_SIN_ACTIVAR_O_SIN_EXISTENCIA.getValue());
        } catch (Exception e) {
            quiteDriver();
            Assertions.fail(e.getMessage(), e);
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("el sistema mostrará un mensaje de error indicando Datos incorrectos o cuenta de usuario no activada.")
    public void elSistemaMostraraUnMensajeDeErrorIndicandoDatosIncorrectosOCuentaDeUsuarioNoActivada() {
        Assertions.assertEquals(MensajesDeError.MENSAJES_DE_ERROR_USUARIO_SIN_ACTIVAR_O_SIN_EXISTENCIA.getValue(), casosFormularioLogin.errorUusarioSinActivarOInexistente());
        quiteDriver();
    }
    private ModeloFormularioLogin formualarioLogin() {
        ModeloFormularioLogin modeloFormularioLogin = new ModeloFormularioLogin();
        modeloFormularioLogin.setEmail("");
        modeloFormularioLogin.setClave(faker.internet().password());
        return modeloFormularioLogin;
    }

    private ModeloFormularioLogin formularioLoginSinContrasenia(){
        ModeloFormularioLogin modeloFormularioLogin = new ModeloFormularioLogin();
        modeloFormularioLogin.setEmail(faker.internet().emailAddress());
        modeloFormularioLogin.setClave(faker.internet().password());
        return modeloFormularioLogin;
    }
}
