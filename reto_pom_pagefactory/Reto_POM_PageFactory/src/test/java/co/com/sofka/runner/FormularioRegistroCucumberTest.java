package co.com.sofka.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        publish = true,
        features = {"src/test/resources/feature/formularioregistro/formularioRegistro.feature",
                "src/test/resources/feature/formularioregistro/formularioRegistroContacto.feature",
                "src/test/resources/feature/formularioregistro/formularioLogin.feature"},
        glue = {"co.com.sofka.pasoapaso.formularioregistro","co.com.sofka.pasoapaso.Login"},
        tags = "@Regresion"
)
public class FormularioRegistroCucumberTest {

}
