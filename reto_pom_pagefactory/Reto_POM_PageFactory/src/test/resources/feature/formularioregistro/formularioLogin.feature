# language: es
Característica: Ingreso de un usuario a la página
  Como usuario de la página Sitio 7
  Necesito loguearme en el sistema
  con el fin de hacer uso de las diferentes funcionalidades que se encuentran en la página web.

  Antecedentes:
    Dado que el usuario se encuentra en la página web en el apartado ingresar

  @Regresion
  Escenario: Ingreso no exitoso de un usario al no ingresar el email
    Cuando El usuario ingresa los campos requeridos pero no el email
    Entonces el sistema mostrará un mensaje de error inicando que falta ingresar el usuario y la contraseña

  @Regresion
  Escenario: Ingreso no exitoso ya que no se ha activado el usuario
    Cuando El usuario ingresa los campos requeridos y valida
    Entonces el sistema mostrará un mensaje de error indicando Datos incorrectos o cuenta de usuario no activada.
