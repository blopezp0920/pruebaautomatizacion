package co.com.sofka.util;

public enum SistemaOperativo {

    SISTEMA_OPERATIVO_LINUX("linux"),
    SITEMA_OPERATIVO(System.getProperty("os.name"));
    private final String value;

    public String getValue() {return value;}
    SistemaOperativo(String value){this.value = value;}
}
