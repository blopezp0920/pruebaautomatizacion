package co.com.sofka.page.comun.page;

import co.com.sofka.modelo.formulariosSitio7.ModeloFormularioContacto;
import co.com.sofka.util.Opciones;
import org.openqa.selenium.WebDriver;

import java.util.Objects;

public class CasosFormularioRegistroContacto extends FormularioRegistroContacto  {
    private final ModeloFormularioContacto modeloFormularioContacto;
    public CasosFormularioRegistroContacto(WebDriver driver, ModeloFormularioContacto modeloFormularioContacto){
        super(driver);
        this.modeloFormularioContacto = modeloFormularioContacto;
    }

    public void camposFormularioContacto(String opcion){

        explicitWait(imagen);
        clickWithJseOn(cerrar);

        clickOn(contacto);
        explicitWait(nombres);

        clearOn(nombres);
        typeOn(nombres, modeloFormularioContacto.getNombreYApellido());

        clearOn(email);
        typeOn(email, modeloFormularioContacto.getEmail());

        clearOn(telefono);
        typeOn(telefono, modeloFormularioContacto.getTelefono());

        clearOn(email);
        typeOn(email, modeloFormularioContacto.getEmail());

        clearOn(empresa);
        typeOn(empresa, modeloFormularioContacto.getEmpresa());

        clearOn(asunto);
        typeOn(asunto, modeloFormularioContacto.getAsunto());

        clearOn(mensaje);
        typeOn(mensaje, modeloFormularioContacto.getMensaje());

        explicitWait(enviar);
        clickWithJseOn(enviar);

        if (Objects.equals(Opciones.OPCION_ASUNTO.getValue(), opcion)){
            explicitWait(asssertionAsuntos);
        }
        if (Objects.equals(Opciones.OPCION_TELEFONO.getValue(), opcion)){
            explicitWait(asssertionsTelefono);
        }
    }
    public String errorAsunto(){
        return getText(asssertionAsuntos);
    }

    public String errorTelefono(){return getText(asssertionsTelefono);
    }

}
