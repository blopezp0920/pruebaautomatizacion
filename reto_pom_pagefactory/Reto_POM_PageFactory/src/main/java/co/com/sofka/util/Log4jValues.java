package co.com.sofka.util;

import java.util.Objects;

public enum Log4jValues {
    LOG4J_PROPERTIES_FILE_PATH(getSistemaOperativo());

    private final String value;
    Log4jValues(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    private static String getSistemaOperativo(){
        if (Objects.equals(SistemaOperativo.SISTEMA_OPERATIVO_LINUX.getValue(), SistemaOperativo.SITEMA_OPERATIVO.getValue())){
           return "/src/main/resources/log4j2.properties";
        }
        return "\\src\\main\\resources\\log4j2.properties";
    }
}
