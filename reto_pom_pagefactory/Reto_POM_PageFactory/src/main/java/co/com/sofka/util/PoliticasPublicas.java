package co.com.sofka.util;

public enum PoliticasPublicas {

    POLITCIAS_PUBLICAS("He leído y acepto las políticas");
    private final String value;

    public String getValue() {return value;}
    PoliticasPublicas(String value){this.value = value;}
}
