package co.com.sofka.page.comun.page;

import co.com.sofka.modelo.formulariosSitio7.ModeloFormularioLogin;
import co.com.sofka.util.Opciones;
import org.openqa.selenium.WebDriver;

import java.util.Objects;

public class CasosFormularioLogin extends FormularioLogin {

    private final ModeloFormularioLogin modeloFormularioLogin;

    public CasosFormularioLogin(WebDriver driver, ModeloFormularioLogin modeloFormularioLogin){
        super(driver);
        this.modeloFormularioLogin = modeloFormularioLogin;
    }
    public void camposFormularioLogin(String opcion){

        explicitWait(imagen);
        clickWithJseOn(cerrar);

        clickOn(ingresar);
        explicitWait(registro);

        clearOn(email);
        typeOn(email, modeloFormularioLogin.getEmail());

        clearOn(password);
        typeOn(password, modeloFormularioLogin.getClave());
        clickOn(submit);

        if (Objects.equals(Opciones.OPCION_EMAIL.getValue(), opcion)){
            explicitWait(assertionErrorEmail);
        }
        if (Objects.equals(Opciones.OPCION_USUARIO_SIN_ACTIVAR_O_SIN_EXISTENCIA.getValue(), opcion)){
            explicitWait(assertionUsuarioSinActivarOInexistente);
        }

    }
    public String errorEmail(){return getText(assertionErrorEmail);}
    public String errorUusarioSinActivarOInexistente(){return getText(assertionUsuarioSinActivarOInexistente);}

}
