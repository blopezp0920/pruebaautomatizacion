package co.com.sofka.page.comun.page;

import co.com.sofka.modelo.formulariosSitio7.ModeloFormularioRegistro;
import co.com.sofka.util.Opciones;
import org.openqa.selenium.WebDriver;

import java.util.Objects;

public class CasosFormularioRegistro extends FormularioRegistro  {
    private final ModeloFormularioRegistro modeloFormularioRegistro;
    public CasosFormularioRegistro(WebDriver driver, ModeloFormularioRegistro modeloFormularioRegistro){
        super(driver);
        this.modeloFormularioRegistro = modeloFormularioRegistro;
    }

    public void camposFormularioRegistroUsuario(String opcion){

        explicitWait(imagen);
        clickWithJseOn(cerrar);

        clickOn(ingresar);
        explicitWait(registro);
        clickOn(registro);

        explicitWait(submit);

        clearOn(nombres);
        typeOn(nombres, modeloFormularioRegistro.getNombre());

        clearOn(apellidos);
        typeOn(apellidos, modeloFormularioRegistro.getApellido());

        clearOn(sitioWeb);
        typeOn(sitioWeb, modeloFormularioRegistro.getSitioWeb());

        clearOn(email);
        typeOn(email, modeloFormularioRegistro.getEmail());

        clearOn(clave);
        typeOn(clave, modeloFormularioRegistro.getClave());

        clearOn(confirmarClave);
        typeOn(confirmarClave, modeloFormularioRegistro.getConfirmarClave());

        clickWithJseOn(politciasPublicas);

        clickOn(submit);

        if (Objects.equals(Opciones.OPCION_NOMBRE.getValue(), opcion)){
            explicitWait(assertionMensajeErrorNombre);
        }

        if (Objects.equals(Opciones.OPCION_CONTRASENIA.getValue(), opcion)){
            explicitWait(assertionMensajeErrorContrasenias);
        }

    }

    public String errorNombre(){
        return getText(assertionMensajeErrorNombre);
    }

    public String errorCampoSinIngresar(){
        return getText( assertionMensajeErrorContrasenias);
    }
}
