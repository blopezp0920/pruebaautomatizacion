package co.com.sofka.util;

public enum MensajesDeError {

    MENSAJES_DE_ERROR_CLAVES("Las claves no coinciden."),
    MENSAJES_DE_ERROR_NOMBRE("Indique su nombre."),
    MENSAJES_DE_ERROR_ASUNTO("Indica el asunto."),
    MENSAJES_DE_ERROR_TELEFONO("Indica tu teléfono."),
    MENSAJE_DE_ERROR_EMAIL_CLAVE("Indica usuario y clave."),
    MENSAJES_DE_ERROR_USUARIO_SIN_ACTIVAR_O_SIN_EXISTENCIA("Datos incorrectos o cuenta de usuario no activada.");
    private final String value;

    public String getValue() {return value;}
    MensajesDeError(String value){this.value = value;}
}
