package co.com.sofka.page.comun;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AccionesComunesEnPages {
    private static final Logger LOGGER = Logger.getLogger(AccionesComunesEnPages.class);

    private WebDriver driver;
    public AccionesComunesEnPages(WebDriver driver) {
        try {
            if(driver == null)
                LOGGER.warn("El Webdriver es nulo.");

            this.driver = driver;

        }catch (Exception e){
            LOGGER.warn(e.getMessage(), e);
        }
    }

    protected void clearOn(WebElement webElement){
        webElement.clear();
    }

    protected void clickOn(WebElement webElement){
        webElement.click();
    }

    protected void typeOn(WebElement webElement, CharSequence... keysToSend){
        webElement.sendKeys(keysToSend);
    }
    protected void clickWithJseOn(WebElement webElement){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click();", webElement);
    }

    protected String getText(WebElement webElement){
        return webElement.getText();
    }

    protected void explicitWait(WebElement webElement){
        new WebDriverWait(driver, Duration.ofSeconds(20))
                .until(ExpectedConditions.elementToBeClickable(webElement));
    }

}
