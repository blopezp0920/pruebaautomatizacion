package co.com.sofka.util;

public enum Opciones {

    OPCION_NOMBRE("Nombre"),
    OPCION_CONTRASENIA("Contrasenia"),
    OPCION_ASUNTO("Asunto"),
    OPCION_TELEFONO("Telefono"),
    OPCION_EMAIL("Email"),
    OPCION_USUARIO_SIN_ACTIVAR_O_SIN_EXISTENCIA("Usuario sin activar o sin existencia");
    private final String value;

    public String getValue() {return value;}
    Opciones(String value){this.value = value;}
}
