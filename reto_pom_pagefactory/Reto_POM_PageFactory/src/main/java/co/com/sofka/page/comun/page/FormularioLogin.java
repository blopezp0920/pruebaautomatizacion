package co.com.sofka.page.comun.page;

import co.com.sofka.page.comun.AccionesComunesEnPages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormularioLogin extends AccionesComunesEnPages {

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"frm_login\"]/div[6]/div[2]/a")
    protected WebElement registro;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"login_usuario\"]")
    protected WebElement email;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"login_clave\"]")
    protected WebElement password;

    @CacheLookup
    @FindBy(id = "btn_login_submit")
    protected WebElement submit;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"img_banner213\"]")
    protected WebElement imagen;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"bs_dialog\"]/div[1]/a")
    protected WebElement cerrar;

    @CacheLookup
    @FindBy(id = "usufoto2")
    protected WebElement ingresar;

    @CacheLookup
    @FindBy(xpath = "/html/body/div[3]/div[2]/div[2]/div/div[2]/div")
    protected WebElement assertionErrorEmail;

    @CacheLookup
    @FindBy(xpath = "/html/body/div[3]/div[2]/div[2]/div/div[2]")
    protected WebElement assertionUsuarioSinActivarOInexistente;

    public FormularioLogin(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
}
