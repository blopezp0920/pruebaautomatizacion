package co.com.sofka.page.comun.page;

import co.com.sofka.page.comun.AccionesComunesEnPages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormularioRegistro extends AccionesComunesEnPages {

    @CacheLookup
    @FindBy(id = "nombres")
    protected WebElement nombres;

    @CacheLookup
    @FindBy(id = "apellidos")
    protected WebElement apellidos;

    //Pais

    @CacheLookup
    @FindBy(id = "sitioweb")
    protected WebElement sitioWeb;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"email\"]")
    protected WebElement email;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"clave\"]")
    protected WebElement clave;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"confirm_clave\"]")
    protected WebElement confirmarClave;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"frmregistro\"]/div[6]/label")
    protected WebElement politciasPublicas;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"img_banner213\"]")
    protected WebElement imagen;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"bs_dialog\"]/div[1]/a")
    protected WebElement cerrar;
    @CacheLookup
    @FindBy(xpath = "/html/body/div[6]/div[2]/div[2]/div/div/div[4]/div/form/div[8]/input")
    protected WebElement submit;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"dhtml_alert_MSG\"]")
    protected WebElement assertionMensajeErrorContrasenias;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"bienvenida\"]/div[1]/div/div[2]/div[2]/div[2]/div[1]/a/span")
    protected WebElement ingresar;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"frm_login\"]/div[6]/div[2]/a")
    protected WebElement registro;

    @CacheLookup
    @FindBy(xpath = "/html/body/div[4]/div[2]/div[2]/div/div[2]/div")
    protected WebElement assertionMensajeErrorNombre;


    public FormularioRegistro(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

}
