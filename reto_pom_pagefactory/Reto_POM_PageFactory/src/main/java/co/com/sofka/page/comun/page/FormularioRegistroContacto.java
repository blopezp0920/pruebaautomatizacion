package co.com.sofka.page.comun.page;

import co.com.sofka.page.comun.AccionesComunesEnPages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormularioRegistroContacto extends AccionesComunesEnPages {


    @CacheLookup
    @FindBy(id = "nombres")
    protected WebElement nombres;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"email\"]")
    protected WebElement email;

    //Pais

    @CacheLookup
    @FindBy(id = "asunto")
    protected WebElement asunto;

    @CacheLookup
    @FindBy(id = "telefono")
    protected WebElement telefono;

    @CacheLookup
    @FindBy(id = "empresa")
    protected WebElement empresa;

    @CacheLookup
    @FindBy(id = "mensaje")
    protected WebElement mensaje;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"btn_submit0\"]")
    protected WebElement enviar;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"img_banner213\"]")
    protected WebElement imagen;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"bs_dialog\"]/div[1]/a")
    protected WebElement cerrar;

    @CacheLookup
    @FindBy(xpath = "/html/body/div[4]/div[2]/div[2]/div/div[2]/div")
    protected WebElement asssertionAsuntos;

    @CacheLookup
    @FindBy(xpath = "/html/body/div[4]/div[2]/div[2]/div/div[2]/div")
    protected WebElement asssertionsTelefono;

    @CacheLookup
    @FindBy(xpath = "//*[@id=\"cssmenu\"]/li[5]/a/span")
    protected WebElement contacto;

    public FormularioRegistroContacto(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
}
