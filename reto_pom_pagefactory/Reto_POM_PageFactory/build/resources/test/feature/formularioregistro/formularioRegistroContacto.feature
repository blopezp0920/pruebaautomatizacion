# language: es
Característica: Registro de un nuevo contacto
  Como usuario de la página Sitio 7
  Necesito registrar un nuevo contacto
  con el fin de contacttarme con el administrador de la página.

  Antecedentes:
    Dado que el usuario se encuentra en la página web en el apartado de contacto

  @Regresion
  Escenario: Registro de un nuevo contacto no exitosa al no ingresar el asunto
    Cuando el usuario suministra los datos pero no el asunto y confirma
    Entonces el sistema deberá mostrar un error asociado al campo asunto faltante.

  @Regresion
  Escenario: Registro de un nuevo contacto no exitoso al no ingresar el télefono
    Cuando el usuario suministra los datos pero no ingresó el télefono
    Entonces el sistema mostrará un mensaje de error indicando que falta suministrar la ifnormación del campo télefono
