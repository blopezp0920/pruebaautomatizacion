# language: es
Característica: Registro de un nuevo usuario
  Como usuario de la página Sitio 7
  Necesito registrarme en el sistema
  con el fin de hacer uso de las diferentes funcionalidades que se encuentran en la página web.

  Antecedentes:
    Dado que el usuario se encuentra en la página web en el apartado registro

  @Regresion
  Escenario: Registro de un nuevo usuario no exitoso al no ingresar el nombre
    Cuando el usuario suministra los datos menos el nombre y confirma
    Entonces el sistema mostrará un mensaje de error indicando que el nombre no ha sido suministrado.

  @Regresion
  Escenario: Registro de un nuevo usuario no exitoso por contraseña erronea
    Cuando el usuario suministra los datos pero no ingresó un las contraseñas iguales
    Entonces el sistema mostrará un mensaje de error indicando que las contraseñas son erroneas.
