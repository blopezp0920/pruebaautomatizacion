package co.com.sofka.utils;

public enum IdsPotsAEliminar {

    ID_USUARIO_A_ACTUALIZAR(1);

    private final int value;

    public int getValue() {
        return value;
    }

    IdsPotsAEliminar(int value) {
        this.value = value;
    }
}
