package co.com.sofka.questions;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Question;

public class PostsQuestions {

    public static Question<String> theExpectedPostsTitle() {
        return actor -> SerenityRest.lastResponse()
                .jsonPath()
                .getString("title");
    }
}
