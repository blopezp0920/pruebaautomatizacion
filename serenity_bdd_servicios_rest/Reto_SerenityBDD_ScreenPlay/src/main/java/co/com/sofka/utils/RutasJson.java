package co.com.sofka.utils;

public enum RutasJson {

    RUTAS_JSON_GET_USUARIO_POR_ID("src/test/resources/archivosjson/GetUsuarioPorId.json"),
    RUTAS_JSON_ACTUALIZAR_UN_USUARIO("src/test/resources/archivosjson/ActualizarUsuario.json"),
    RUTAS_JSON_NUEVO_POST ("src/test/resources/archivosjson/NuevoPost.json");

    private final String value;

    public String getValue() {
        return value;
    }

    RutasJson(String value) {
        this.value = value;
    }

}
