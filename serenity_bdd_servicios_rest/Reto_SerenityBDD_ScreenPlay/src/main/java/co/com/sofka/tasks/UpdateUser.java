package co.com.sofka.tasks;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Put;
import org.json.simple.JSONObject;

public class UpdateUser {


    private UpdateUser() {
        // Constructor updateUser
    }

    public static Performable withId(int id, JSONObject jsonObject) {
        return  Task.where(
                Put.to("/users/"+id)
                        .with(request -> request.header("Content-Type", "application/json")
                                .body(jsonObject)
                        )
        );
    }

}
