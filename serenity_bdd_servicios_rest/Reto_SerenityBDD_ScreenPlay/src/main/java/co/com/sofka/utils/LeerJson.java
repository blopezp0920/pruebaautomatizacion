package co.com.sofka.utils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class LeerJson {


    public JSONObject obtenerUsuarioJson(String rutasJsonGetUsuarioPorId) throws IOException, ParseException {

        Object jsonUsuario = new JSONParser().parse(new FileReader(rutasJsonGetUsuarioPorId));

        return (JSONObject) jsonUsuario;
    }


}
