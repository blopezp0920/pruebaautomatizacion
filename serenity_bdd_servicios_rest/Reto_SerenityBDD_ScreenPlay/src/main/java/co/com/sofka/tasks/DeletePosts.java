package co.com.sofka.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Delete;

public class DeletePosts implements Task {

    private String resource;
    private String id;

    public DeletePosts usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public DeletePosts withId(String id) {
        this.id = id;
        return this;
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Delete.from(resource+id)
        );
    }

    public static DeletePosts deletePosts(){
        return new DeletePosts();
    }
}
