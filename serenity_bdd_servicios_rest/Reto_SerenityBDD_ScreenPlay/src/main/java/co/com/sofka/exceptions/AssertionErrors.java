package co.com.sofka.exceptions;

public class AssertionErrors extends AssertionError{

    public static final String POST_NOT_SAVED = "Post not saved correctly";
    public static final String USER_NOT_DOUND = "User not found";
    public static final String STATUS_ERROR = "The response code is not as expected";

    public AssertionErrors(String message) {
        super(message);
    }


}
