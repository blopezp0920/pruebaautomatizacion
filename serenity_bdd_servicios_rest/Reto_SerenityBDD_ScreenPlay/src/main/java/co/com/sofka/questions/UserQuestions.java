package co.com.sofka.questions;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Question;

public class UserQuestions {
    private UserQuestions() {
    }
    public static Question<String> theExpectedUserName() {
        return actor -> SerenityRest.lastResponse()
                .jsonPath()
                .getString("first_name");
    }

}
