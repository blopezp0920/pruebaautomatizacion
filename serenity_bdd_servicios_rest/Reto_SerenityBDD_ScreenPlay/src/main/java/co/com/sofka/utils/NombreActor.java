package co.com.sofka.utils;

public enum NombreActor {

    NOMBRE_ACTOR("Bryan");

    private final String value;

    public String getValue() {
        return value;
    }

    NombreActor(String value) {
        this.value = value;
    }

}
