package co.com.sofka.utils;

public enum IdsUsuariosActualizar {

    ID_USUARIO_A_ACTUALIZAR(2);

    private final int value;

    public int getValue() {
        return value;
    }

    IdsUsuariosActualizar(int value) {
        this.value = value;
    }
}
