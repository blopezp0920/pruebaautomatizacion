package co.com.sofka.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.json.simple.JSONObject;

import java.util.HashMap;

public class CreatePost implements Task {

    private String resource;
    private HashMap<String, Object> headers = new HashMap<>();
    private JSONObject bodyRequest;

    public CreatePost usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }

    public CreatePost withHeaders(HashMap<String, Object> headers) {
        this.headers = headers;
        return this;
    }

    public CreatePost andBodyRequest(JSONObject bodyRequest) {
        this.bodyRequest = bodyRequest;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(resource).with(
                        requestSpecification -> requestSpecification.relaxedHTTPSValidation()
                                .headers(headers)
                                .body(bodyRequest)

                )
        );
    }

    public static CreatePost createPost(){
        return new CreatePost();
    }


}
