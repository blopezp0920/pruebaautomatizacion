#language: es

Característica: Actualizar un usuario
    Como administrador
    Necesito actualizar un usuario
    Para actualizar información de algún usuario

    Escenario: Actualizar campos de algún usuario
        Dado que el administrador se encuentre logueado
        Cuando el administrador busca el usuario a actualizar y suministra los campos a actualizarse
        Entonces el sistema retornará la nueva información del usuario actualizado