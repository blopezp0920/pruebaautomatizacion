#language: es

  Característica: Buscar usuario por número de indentificación
    Como administrador
    Necesito buscar usuarios por su número de identificación
    Para obtener la información detallada del usuario buscado

    Escenario: Buscar usuario por su respectivo número de identificación
      Dado que el administrador está en el recurso web indicando el número de identificación
      Cuando el administrador genera la consulta
      Entonces se visualizará la información detallada del usuario buscado