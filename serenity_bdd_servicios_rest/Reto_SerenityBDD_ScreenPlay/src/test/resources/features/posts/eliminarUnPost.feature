#language: es

Característica: Eliminar un Post
  Como administrador registrado en el sistema
  necesito eliminar un Post
  para borrar algún registro no deseado

  Escenario: Eliminar un Post de manera exitosa
    Dado que el administrador se encuentra logueado en la paǵina
    Cuando el administrador proporciona el Post a eliminar y valida
    Entonces el sistema devolverá un status OK de que el registro ha sido eliminado