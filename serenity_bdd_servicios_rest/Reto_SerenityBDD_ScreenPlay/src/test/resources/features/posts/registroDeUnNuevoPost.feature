#language: es

Característica: Registro de un nuevo post
  Como usuario registrado en el sistema
  necesito registrar un nuevo post
  para ingresar la información pertinente del post

  @Regresion
  Escenario: Registro de un nuevo Post exitoso
    Dado que el usuario se encuentra en el apartado de registro de un nuevo post
    Cuando el usuario hace la petición de registrar el nuevo post
    Entonces El sistema retornará la información ingresada indicando una respuesta exitosa

