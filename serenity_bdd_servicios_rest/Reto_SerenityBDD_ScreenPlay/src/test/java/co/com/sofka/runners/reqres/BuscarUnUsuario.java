package co.com.sofka.runners.reqres;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/reqres/buscarUnUsuario.feature"},
        glue = {"co.com.sofka.stepdefinitions"}
)

public class BuscarUnUsuario {
}
