package co.com.sofka.stepdefinitions.reqres;

import co.com.sofka.exceptions.AssertionErrors;
import co.com.sofka.tasks.UpdateUser;
import co.com.sofka.utils.IdsUsuariosActualizar;
import co.com.sofka.utils.LeerJson;
import co.com.sofka.utils.NombreActor;
import co.com.sofka.utils.RutasJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static co.com.sofka.exceptions.AssertionErrors.USER_NOT_DOUND;
import static co.com.sofka.questions.UserQuestions.theExpectedUserName;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ActualizarUnUsuarioStepDefinition {

    LeerJson objetoJsonUsuario = new LeerJson();
    JSONObject jsonUsuario = objetoJsonUsuario.obtenerUsuarioJson(RutasJson.RUTAS_JSON_ACTUALIZAR_UN_USUARIO.getValue());
    private static final String URL_BASE = "https://reqres.in/api";

    public ActualizarUnUsuarioStepDefinition() throws IOException, ParseException {
    }

    @Dado("que el administrador se encuentre logueado")
    public void queElAdministradorSeEncuentreLogueado() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled(NombreActor.NOMBRE_ACTOR.getValue());
        theActorInTheSpotlight().whoCan(CallAnApi.at(URL_BASE));

    }
    @Cuando("el administrador busca el usuario a actualizar y suministra los campos a actualizarse")
    public void elAdministradorBuscaElUsuarioAActualizarYSuministraLosCamposAActualizarse() {

        theActorInTheSpotlight().attemptsTo(
                UpdateUser.withId(IdsUsuariosActualizar.ID_USUARIO_A_ACTUALIZAR.getValue(),jsonUsuario)
        );
    }
    @Entonces("el sistema retornará la nueva información del usuario actualizado")
    public void elSistemaRetornaraLaNuevaInformacionDelUsuarioActualizado() {
        String Response = new String(LastResponse.received().answeredBy(theActorInTheSpotlight()).asByteArray(), StandardCharsets.UTF_8);
        theActorInTheSpotlight().should(
                seeThat(Response,
                        theExpectedUserName(),equalTo(jsonUsuario.get("first_name")))
                        .orComplainWith(AssertionErrors.class, USER_NOT_DOUND)
        );
    }

}
