package co.com.sofka.stepdefinitions.reqres;


import co.com.sofka.tasks.FindAUser;
import co.com.sofka.utils.LeerJson;
import co.com.sofka.utils.NombreActor;
import co.com.sofka.utils.RutasJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;

import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static co.com.sofka.utils.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;
import static com.google.common.base.StandardSystemProperty.USER_DIR;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.core.IsEqual.equalTo;


public class BuscarUnUsuarioStepDefinition {

    LeerJson objetoJsonUsuario = new LeerJson();
    JSONObject jsonUsuario = (JSONObject) objetoJsonUsuario.obtenerUsuarioJson(RutasJson.RUTAS_JSON_GET_USUARIO_POR_ID.getValue()).get("data");
    private final String ID_USUARIO = jsonUsuario.get("id").toString();
    private static final String URL_BASE = "https://reqres.in/api";
    private final Actor actor = Actor.named(NombreActor.NOMBRE_ACTOR.getValue());

    public BuscarUnUsuarioStepDefinition() throws IOException, ParseException {
    }

    @Dado("que el administrador está en el recurso web indicando el número de identificación")
    public void queElAdministradorEstaEnElRecursoWebIndicandoElNumeroDeIdentificacion() {
        setUpLog4j2();
        actor.can(CallAnApi.at(URL_BASE));
    }
    @Cuando("el administrador genera la consulta")
    public void elAdministradorGeneraLaConsulta() {

        actor.attemptsTo(
                FindAUser.withId(Integer.parseInt(ID_USUARIO))
        );

    }

    @Entonces("se visualizará la información detallada del usuario buscado")
    public void seVisualizaraLaInformacionDetalladaDelUsuarioBuscado() {
        String ResponseBooking = new String(String.valueOf(LastResponse.received().answeredBy(actor)));
        actor.should(
                seeThatResponse("El código de respuesta debe ser: " + HttpStatus.SC_OK + jsonUsuario.toJSONString(),
                        validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)
                                .body("data.id",equalTo(Integer.parseInt(ID_USUARIO)))
                                .body("data.first_name", equalTo(jsonUsuario.get("first_name")))
                                .body("data.last_name", equalTo(jsonUsuario.get("last_name")))
                                .log()
                )
        );
    }

    private void setUpLog4j2(){
        PropertyConfigurator.configure(USER_DIR.value() + LOG4J_PROPERTIES_FILE_PATH.getValue());
    }

}
