package co.com.sofka.stepdefinitions.posts;

import co.com.sofka.exceptions.AssertionErrors;
import co.com.sofka.utils.IdsPotsAEliminar;
import co.com.sofka.utils.NombreActor;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;

import static co.com.sofka.exceptions.AssertionErrors.STATUS_ERROR;
import static co.com.sofka.tasks.DeletePosts.deletePosts;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class EliminarUnPostsStepDefinition {

    private static final String URL_BASE = "https://jsonplaceholder.typicode.com";
    private static final String RESOURCE = "/posts";
    private final String ID_POSTS= "/"+IdsPotsAEliminar.ID_USUARIO_A_ACTUALIZAR.getValue();

    public EliminarUnPostsStepDefinition() {
    }

    @Dado("que el administrador se encuentra logueado en la paǵina")
    public void queElAdministradorSeEncuentraLogueadoEnLaPagina() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled(NombreActor.NOMBRE_ACTOR.getValue());
        theActorInTheSpotlight().whoCan(CallAnApi.at(URL_BASE));

    }
    @Cuando("el administrador proporciona el Post a eliminar y valida")
    public void elAdministradorProporcionaElPostAEliminarYValida() {
        theActorInTheSpotlight().attemptsTo(
                deletePosts()
                        .usingTheResource(RESOURCE)
                        .withId(ID_POSTS)
        );

    }
    @Entonces("el sistema devolverá un status OK de que el registro ha sido eliminado")
    public void elSistemaDevolveraUnStatusOKDeQueElRegistroHaSidoEliminado() {
        theActorInTheSpotlight().should(
                seeThatResponse("Eliminado con éxito",
                        response -> response.statusCode(HttpStatus.SC_OK)
                        )
                        .orComplainWith(AssertionErrors.class, STATUS_ERROR)
        );

    }
}
