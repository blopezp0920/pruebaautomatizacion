package co.com.sofka.stepdefinitions.posts;
import co.com.sofka.exceptions.AssertionErrors;
import co.com.sofka.utils.LeerJson;
import co.com.sofka.utils.NombreActor;
import co.com.sofka.utils.RutasJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import static co.com.sofka.exceptions.AssertionErrors.POST_NOT_SAVED;
import static co.com.sofka.questions.PostsQuestions.theExpectedPostsTitle;
import static co.com.sofka.tasks.CreatePost.createPost;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.text.IsEmptyString.isEmptyString;

public class IngresarUnNuevoPostStepDefinition {


    private static final String URL_BASE = "https://jsonplaceholder.typicode.com";
    private static final String RESOURCE = "/posts";
    private final HashMap<String, Object> headers = new HashMap<>();
    LeerJson objetoPost= new LeerJson();
    JSONObject jsonPost = objetoPost.obtenerUsuarioJson(RutasJson.RUTAS_JSON_NUEVO_POST.getValue());

    public IngresarUnNuevoPostStepDefinition() throws IOException, ParseException {
    }

    @Dado("que el usuario se encuentra en el apartado de registro de un nuevo post")
    public void queElUsuarioSeEncuentraEnElApartadoDeRegistroDeUnNuevoPost() {

        OnStage.setTheStage(new OnlineCast());
        theActorCalled(NombreActor.NOMBRE_ACTOR.getValue());
        theActorInTheSpotlight().whoCan(CallAnApi.at(URL_BASE));
        headers.put("Content-Type","application/json");
    }
    @Cuando("el usuario hace la petición de registrar el nuevo post")
    public void elUsuarioHaceLaPeticionDeRegistrarElNuevoPost() {

        theActorInTheSpotlight().attemptsTo(
              createPost()
                      .usingTheResource(RESOURCE)
                      .withHeaders(headers)
                      .andBodyRequest(jsonPost)

        );
    }
    @Entonces("El sistema retornará la información ingresada indicando una respuesta exitosa")
    public void elSistemaRetornaraLaInformacionIngresadaIndicandoUnaRespuestaExitosa() {
        String ResponseBooking = new String(LastResponse.received().answeredBy(theActorInTheSpotlight()).asByteArray(), StandardCharsets.UTF_8);
        theActorInTheSpotlight().should(
                seeThatResponse(ResponseBooking,
                        response -> response.statusCode(HttpStatus.SC_CREATED)
                                .body(theExpectedPostsTitle().getSubject(),not(isEmptyString())))
                        .orComplainWith(AssertionErrors.class, POST_NOT_SAVED)
        );

    }
}
