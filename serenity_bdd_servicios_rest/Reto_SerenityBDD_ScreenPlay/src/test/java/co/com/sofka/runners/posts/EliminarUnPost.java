package co.com.sofka.runners.posts;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/posts/eliminarUnPost.feature"},
        glue = {"co.com.sofka.stepdefinitions.posts"}

)
public class EliminarUnPost {
}
